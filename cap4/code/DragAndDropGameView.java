package toymobi.eduardo.dragdropgame;

import ...

public class DragAndDropGameView extends RelativeLayout implements OnTouchListener {

    public static final int[] SPOTS_RES_IDS = { R.id.spot1, R.id.spot2,
            R.id.spot3, R.id.spot4, R.id.spot5, R.id.spot6, R.id.spot7,
            R.id.spot8, R.id.spot9 };

    public static final int MAX_LETTER_SPOT = SPOTS_RES_IDS.length;

    private static final int OFFSET_TOUCH_POSITIVE = 32;

    private static final int OFFSET_TOUCH_NEGATIVE = -32;

    private SparseArrayCompat<ImageView> spotItems = null;

    private View selectedItemView = null;

    private int offsetX = 0;

    private int offsetY = 0;

    private boolean isTouch = false;

    private boolean isDrop = false;

    private android.view.ViewGroup.LayoutParams imageParams,
            imageParamsLetter1, imageParamsLetter2, imageParamsLetter3;

    private ImageView letter1, letter2, letter3;

    private int crashX, crashY;

    private int topy, leftX, rightX, bottomY;

    private int indexSpot = 0;

    private CharSequence checkCurrentLetterTag;

    private DragItem dragItem;

    public OnNextItemListener onNextItemListener;

    public DragAndDropGameView (final Context context) {
        super(context);
    }

    public DragAndDropGameView (final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public DragAndDropGameView (final Context context,
            final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onFinishInflate () {
        super.onFinishInflate();
        init(getContext());
    }

    private void init (final Context context) {
        if (!isInEditMode() && context != null) {

            inflate(context, R.layout.drag_and_drop_layout, this);

            if (spotItems == null) {

                final int lenght = SPOTS_RES_IDS.length;

                spotItems = new SparseArrayCompat<ImageView>(lenght);

                for (int i = 0; i < lenght; i++) {
                    final ImageView spot = (ImageView) findViewById(SPOTS_RES_IDS[i]);
                    if (spot != null) {
                        spotItems.put(i, spot);
                    }
                }
            }

            final ViewGroup container = (ViewGroup) findViewById(R.id.container);

            letter1 = (ImageView) findViewById(R.id.letter_1);

            letter2 = (ImageView) findViewById(R.id.letter_2);

            letter3 = (ImageView) findViewById(R.id.letter_3);

            final RelativeLayout.LayoutParams letterDragLayoutParams = new RelativeLayout.LayoutParams(
                    new ViewGroup.MarginLayoutParams(
                            RelativeLayout.LayoutParams.WRAP_CONTENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT));

            container.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);

            final int widthContainer = DeviceDimensionsUtil.getDisplayWidth(context);
            
            final int heightContainer = DeviceDimensionsUtil
                    .getDisplayHeight(context);

            final Rect rect = new Rect(OFFSET_TOUCH_NEGATIVE,
                    OFFSET_TOUCH_NEGATIVE, widthContainer
                            - OFFSET_TOUCH_POSITIVE, heightContainer
                            - OFFSET_TOUCH_POSITIVE);

            container.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch (final View view, final MotionEvent event) {

                    if (isTouch) {

                        final ImageView spotByIndex = spotItems.get(indexSpot);

                        switch (event.getActionMasked()) {

                        case MotionEvent.ACTION_DOWN:

                            if (spotByIndex != null) {

                                topy = spotByIndex.getTop();
                                leftX = spotByIndex.getLeft();
                                rightX = spotByIndex.getRight();
                                bottomY = spotByIndex.getBottom();
                            }

                            break;

                        case MotionEvent.ACTION_MOVE:
                            crashX = (int) event.getX();
                            crashY = (int) event.getY();

                            final int x = (int) event.getX() - offsetX;
                            final int y = (int) event.getY() - offsetY;

                            letterDragLayoutParams.setMargins(x, y, 0, 0);

                            // Drop Image Here
                            if (crashX > leftX && crashX < rightX
                                    && crashY > topy && crashY < bottomY) {

                                checkCurrentLetterTag = (CharSequence) selectedItemView
                                        .getTag();

                                if (checkCurrentLetterTag
                                        .equals(DragItem.CORRECT)) {
                                    if (dragItem != null) {
                                        if (spotByIndex != null) {

                                            spotByIndex
                                                    .setImageResource(dragItem.letterImageCorrect);

                                            if (onNextItemListener != null) {
                                                onNextItemListener.nextItem();
                                            }
                                        }
                                        isDrop = true;

                                        if (selectedItemView != null) {
                                            selectedItemView
                                                    .setVisibility(View.INVISIBLE);
                                        }
                                    }
                                } else {
                                    event.setAction(MotionEvent.ACTION_UP);
                                    onTouch(view, event);
                                }
                            } else {
                                if (selectedItemView != null) {
                                    // Drop Image Here
                                    if (rect != null && rect.contains(x, y)) {
                                        selectedItemView
                                                .setLayoutParams(letterDragLayoutParams);
                                    } else {
                                        event.setAction(MotionEvent.ACTION_UP);
                                        onTouch(view, event);
                                    }
                                }
                            }
                            break;
                        case MotionEvent.ACTION_UP:
                            isTouch = false;
                            if (isDrop) {
                                isDrop = false;
                            } else {
                                selectedItemView.setLayoutParams(imageParams);
                            }
                            break;
                        }
                    }
                    return true;
                }
            });

            letter1.setOnTouchListener(this);
            letter2.setOnTouchListener(this);
            letter3.setOnTouchListener(this);

            imageParamsLetter1 = letter1.getLayoutParams();
            imageParamsLetter2 = letter2.getLayoutParams();
            imageParamsLetter3 = letter3.getLayoutParams();
        }
    }

    @Override
    public boolean onTouch (final View view, final MotionEvent event) {
        switch (event.getActionMasked()) {
        case MotionEvent.ACTION_DOWN:
            isTouch = true;
            offsetX = (int) event.getX();
            offsetY = (int) event.getY();
            selectedItemView = view;
            imageParams = view.getLayoutParams();
            break;
        case MotionEvent.ACTION_UP:
            selectedItemView = null;
            isTouch = false;
            break;
        default:
            break;
        }
        return false;
    }

    public final void deallocate () {

        if (letter1 != null) {
            letter1.destroyDrawingCache();
            letter1.setImageBitmap(null);
            letter1 = null;
        }

        if (letter2 != null) {
            letter2.destroyDrawingCache();
            letter2.setImageBitmap(null);
            letter2 = null;
        }

        if (letter3 != null) {
            letter3.destroyDrawingCache();
            letter3.setImageBitmap(null);
            letter3 = null;
        }

        if (spotItems != null) {
            final int lenght = SPOTS_RES_IDS.length;

            for (int i = 0; i < lenght; i++) {
                ImageView spot = (ImageView) findViewById(SPOTS_RES_IDS[i]);
                if (spot != null) {
                    spot.destroyDrawingCache();
                    spot.setImageBitmap(null);
                    spot = null;
                }
            }
            spotItems.clear();
            spotItems = null;
        }
    }

    public final void setDragItem (final DragItem dragItem) {
        this.dragItem = dragItem;
    }

    private final void setAttributesIA () {

        if (dragItem != null) {

            indexSpot = dragItem.indexGoalSpotUp;

            for (int i = 0; i < dragItem.lengthName; i++) {
                if (i != indexSpot) {
                    final ImageView spot = spotItems.get(i);
                    if (spot != null) {
                        spot.setImageResource(dragItem.itemsLetter.get(i).letterImageId);
                    }
                } else {

                    switch (dragItem.indexGoalSpotDown) {

                    case DragItem.LETTER_1:

                        letter1.setTag(DragItem.CORRECT);

                        letter2.setTag(DragItem.INCORRECT);

                        letter3.setTag(DragItem.INCORRECT);

                        letter1.destroyDrawingCache();
                        letter1.setImageDrawable(null);
                        letter1.setBackgroundResource(dragItem.letterImageCorrect);

                        letter2.destroyDrawingCache();
                        letter2.setImageDrawable(null);
                        letter2.setImageResource(dragItem.letterImageIncorrectCorrect1);

                        letter3.destroyDrawingCache();
                        letter3.setImageDrawable(null);
                        letter3.setImageResource(dragItem.letterImageIncorrectCorrect2);

                        break;

                    case DragItem.LETTER_2:

                        letter1.setTag(DragItem.INCORRECT);

                        letter2.setTag(DragItem.CORRECT);

                        letter3.setTag(DragItem.INCORRECT);

                        letter2.destroyDrawingCache();
                        letter2.setImageDrawable(null);
                        letter2.setImageResource(dragItem.letterImageCorrect);

                        letter1.destroyDrawingCache();
                        letter1.setImageDrawable(null);
                        letter1.setImageResource(dragItem.letterImageIncorrectCorrect1);

                        letter3.destroyDrawingCache();
                        letter3.setImageDrawable(null);
                        letter3.setImageResource(dragItem.letterImageIncorrectCorrect2);

                        break;

                    case DragItem.LETTER_3:

                        letter1.setTag(DragItem.INCORRECT);

                        letter2.setTag(DragItem.INCORRECT);

                        letter3.setTag(DragItem.CORRECT);

                        letter3.destroyDrawingCache();
                        letter3.setImageDrawable(null);
                        letter3.setImageResource(dragItem.letterImageCorrect);

                        letter1.destroyDrawingCache();
                        letter1.setImageDrawable(null);
                        letter1.setImageResource(dragItem.letterImageIncorrectCorrect1);

                        letter2.destroyDrawingCache();
                        letter2.setImageDrawable(null);
                        letter2.setImageResource(dragItem.letterImageIncorrectCorrect2);
                        break;

                    default:
                        break;
                    }
                }
            }

            for (int i = dragItem.lengthName; i < spotItems.size(); i++) {
                final ImageView spot = spotItems.get(i);
                if (spot != null) {
                    spot.setVisibility(View.GONE);
                }
            }
        }
    }

    public final void eraserSpot () {
        if (spotItems != null) {
            final int lenght = SPOTS_RES_IDS.length;

            for (int i = 0; i < lenght; i++) {
                final ImageView spot = spotItems.get(i);
                if (spot != null) {
                    spot.destroyDrawingCache();
                    spot.setImageDrawable(null);
                    spot.setBackgroundResource(R.drawable.image_spot);
                }
            }
        }
        selectedItemView = null;
        isTouch = false;
        isDrop = false;

        letter1.setVisibility(VISIBLE);
        letter2.setVisibility(VISIBLE);
        letter3.setVisibility(VISIBLE);

        letter1.setLayoutParams(imageParamsLetter1);
        letter2.setLayoutParams(imageParamsLetter2);
        letter3.setLayoutParams(imageParamsLetter3);

        for (int i = 0; i < spotItems.size(); i++) {
            final ImageView spot = spotItems.get(i);
            if (spot != null) {
                spot.setVisibility(View.VISIBLE);
            }
        }
    }

    public final void start () {
        if (dragItem != null) {
            dragItem.init();
            setAttributesIA();
        }
    }
}
