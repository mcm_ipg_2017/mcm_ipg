// Exemplo do uso da classe SimpleSoundPool
// Criar um pool com 3 efeitos sonoros 
// Para tocar o som usa-se o id como referencia
SimpleSoundPool sfx = new SimpleSoundPool(context, R.raw.sfx_normal_click, R.raw.sfx_winner, R.raw.sfx_lose);

sfx.playSound(R.raw.sfx_normal_click);

sfx.playSound(R.raw.sfx_winner);