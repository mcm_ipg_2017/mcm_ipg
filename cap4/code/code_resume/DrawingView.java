public class DrawingView extends View {
   
    public static final int DRAW_DOT = 0;
    public static final int DRAW_SQUARE = DRAW_DOT + 1;
    public static final int DRAW_CIRCLE = DRAW_SQUARE + 1;
    public static final int DRAW_TRIANGLE = DRAW_CIRCLE + 1;

    private static final int SHAPE_STROKE_SIZE = 6;

    public int drawType = DRAW_DOT;

    private SimpleSoundPool sfx;

    private Path drawPath, poligonTemp;

    private Matrix matrix;

    private Paint drawPaint, canvasPaint;

    private int paintColor = 0xFFFFFFFF;

    private Canvas drawCanvas;

    private Bitmap canvasBitmap;

    private float brushSize, lastBrushSize;

    private boolean erase = false;

    public DrawingView (final Context context) { ... }

    public DrawingView (final Context context, final AttributeSet attrs, final int defStyle) { ... }

    public DrawingView (final Context context, final AttributeSet attrs) { ... }

    private final void init (final Context context) {
        if (!isInEditMode()) {
            if (context != null) {
                if (sfx == null) {
                    sfx = new SimpleSoundPool(context, R.raw.sfx_drawing);
                }
            }
            brushSize = getResources().getInteger(R.integer.medium_size);
            lastBrushSize = brushSize;
            drawPath = new Path();
            drawPaint = new Paint();
            drawPaint.setColor(paintColor);
            drawPaint.setAntiAlias(true);
            drawPaint.setStrokeWidth(brushSize);
            drawPaint.setStyle(Paint.Style.STROKE);
            drawPaint.setStrokeJoin(Paint.Join.ROUND);
            drawPaint.setStrokeCap(Paint.Cap.ROUND);
            canvasPaint = new Paint(Paint.DITHER_FLAG);
        }
    }

    @Override
    public final boolean onTouchEvent (final MotionEvent event) {
        final float touchX = event.getX();
        final float touchY = event.getY();

        switch (event.getAction()) {
        case MotionEvent.ACTION_DOWN:
            if (drawType == DRAW_DOT) {
                drawPath.moveTo(touchX, touchY);
                drawPath.lineTo(touchX + 1, touchY + 1);
                invalidate();
            } else {
                drawShape(touchX, touchY);
            }
            return true;
        case MotionEvent.ACTION_MOVE:
            if (drawType == DRAW_DOT) {
                drawPath.lineTo(touchX, touchY);
                invalidate();
            } else {
                drawShape(touchX, touchY);
            }

            return true;
        case MotionEvent.ACTION_UP:
            if (sfx != null && GlobalSettings.soundEnable) {
                sfx.playSound(R.raw.sfx_drawing);
            }

            if (drawType == DRAW_DOT) {
                drawShape(touchX, touchY);
            }
            return true;
        default:
            return super.onTouchEvent(event);
        }
    }

    private final void drawShape (final float touchX, final float touchY) {
        switch (drawType) {
        
		case DRAW_DOT:
            drawCanvas.drawPath(drawPath, drawPaint);
            drawPath.reset();
            break;

        case DRAW_SQUARE:
            drawPaint.setStrokeWidth(SHAPE_STROKE_SIZE);
            drawPath.addPath(getPoligon(touchX, touchY, (int) brushSize, 4, 45f));
            drawCanvas.drawPath(drawPath, drawPaint);
            drawPath.reset();
            break;

        case DRAW_CIRCLE:
            drawPaint.setStrokeWidth(SHAPE_STROKE_SIZE);
            drawPath.addCircle(touchX, touchY, brushSize, Path.Direction.CCW);
            drawCanvas.drawPath(drawPath, drawPaint);
            drawPath.reset();
            break;

        case DRAW_TRIANGLE:
            drawPaint.setStrokeWidth(SHAPE_STROKE_SIZE);
            drawPath.addPath(getPoligon(touchX, touchY, (int) brushSize, 3, 30f));
            drawCanvas.drawPath(drawPath, drawPaint);
            drawPath.reset();
            break;
        }
        invalidate();
    }

	@Override
    protected final void onSizeChanged (final int width, final int heigth, final int oldWidth, final int oldHeigth) { ... }

    @Override
    protected final void onDraw (final Canvas canvas) { ... }
	
    public final void setColor (final int newColor) { ... }

    public final void setBrushSize (final float newSize) { ... }

    public final void setLastBrushSize (final float lastSize) { ... }
	
    public final float getLastBrushSize () { ... }

    public final void setErase (final boolean isErase) { ... }

    public final void startNewPaper () { ... }

    private final Path getPoligon (final float centerX, final float centerY, final int radius, final int numberPoints, final float degrees) { ... }
}
