public class VibrationFeedback {

    private Vibrator vibrator;

    private static final long VIBRATE_MILLISECONDS_DEFAULT = 100;

    public VibrationFeedback(@NonNull final Context context) {
		
        if (vibrator == null) {
        
			if (checkVibratePermission(context)) {
                vibrator = (Vibrator) context.
										getSystemService(Context.VIBRATOR_SERVICE);
            }
			
        }
    }

    private boolean checkVibratePermission(@NonNull final Context context) {
		
        boolean result = false;
        
		final String permission = "android.permission.VIBRATE";
        
		final int res = context.checkCallingOrSelfPermission(permission);
        
		result = (res == PackageManager.PERMISSION_GRANTED);
        
		return result;
    }

    public final void vibrate(final long milliseconds) {
		
        if (vibrator != null && milliseconds > 0) {
            vibrator.vibrate(milliseconds);
        }
    }

    public final void vibrate() {
		
        if (vibrator != null) {
            vibrator.vibrate(VIBRATE_MILLISECONDS_DEFAULT);
        }
    }
}