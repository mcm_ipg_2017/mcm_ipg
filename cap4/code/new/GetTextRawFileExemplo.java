// Exemplo para carregar todas as paginas do livro em ficheiros separados 
private final void createPageText(@NonNull final Resources res) {
	if (pageText == null) {
		final int pageLength = BookResources.BOOK_PAGES_TEXT.length;
		pageText = new SparseArrayCompat<CharSequence>(pageLength);
		final int size = BookResources.BOOK_PAGES_TEXT.length;

		for (int i = 0; i < size; i++) {
			final int pageIndexID = BookResources.BOOK_PAGES_TEXT[i];
			final CharSequence page = GetTextRawFile.getTextRawFile(res, pageIndexID);

			if (page != null) {
				pageText.put(i, page);
			}
		}
	}
}