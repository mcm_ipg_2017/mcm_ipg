	public final void setTTS () {
		if (imageViewSPC != null && imageViewSPC.size() > 0) {

            final int length = imageViewSPC.size();

            for (int i = 0; i < length; i++) {
				
                final ImageViewAnimationTTS imageViewAnimationTTS = imageViewSPC.get(i);

                if (imageViewAnimationTTS != null) {
                    imageViewAnimationTTS.setTextToSpeech(textToSpeech);
                }
            }
        }
    }

    private void stopTTS () {
        if (EduardoStuff.ENABLE_TTS && textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
    }

    private void createItemsTTS () {
        if (adapter != null) {
            if (imageViewSPC == null) {
				
                imageViewSPC = new ArrayList<ImageViewAnimationTTS>();

                for (int i = 0; i < BookSPCResources.SPC_BUTTONS_PAGE_1_LENGTH; i++) {

                    final ImageViewAnimationTTS temp = (ImageViewAnimationTTS) adapter.getViewByIndex(0).findViewById(BookSPCResources.SPC_BUTTONS_PAGE_1[i]);

                    if (temp != null) {
                        imageViewSPC.add(temp);
                    }
                }

                for (int i = 0; i < BookSPCResources.SPC_BUTTONS_PAGE_2_LENGTH; i++) {

                    final ImageViewAnimationTTS temp = (ImageViewAnimationTTS) adapter.getViewByIndex(1).findViewById(BookSPCResources.SPC_BUTTONS_PAGE_2[i]);

                    if (temp != null) {
                        imageViewSPC.add(temp);
                    }
                }
				(...)               
            }
        }
    }
}
