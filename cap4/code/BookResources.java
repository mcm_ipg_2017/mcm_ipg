package toymobi.eduardo.book;

public interface BookResources {

    public static final int[] BOOK_PAGES_TEXT = { R.raw.book_page_01,
            R.raw.book_page_02, R.raw.book_page_03, R.raw.book_page_04,
            R.raw.book_page_05, R.raw.book_page_06, R.raw.book_page_07,
            BookResources.PAGE_WITHOUT_NARRATION };

    public static final int BOOK_PAGES_SIZE = BOOK_PAGES_TEXT.length;

    public static final int[] BOOK_PAGES_IMAGE = { R.drawable.page_1,
            R.drawable.page_2, R.drawable.page_3, R.drawable.page_4,
            R.drawable.page_5, R.drawable.page_6, R.drawable.page_7,
            R.drawable.page_8 };

    public static final int[] BOOK_PAGES_LAYOUT = { R.layout.book_simple_01,
            R.layout.book_simple_02, R.layout.book_simple_03,
            R.layout.book_simple_04, R.layout.book_simple_05,
            R.layout.book_simple_06, R.layout.book_simple_07,
            R.layout.book_simple_end };

    public static final int PAGE_WITHOUT_NARRATION = -1;

    public static final int[] audiosPath = { R.string.narration_page_1,
            R.string.narration_page_2, R.string.narration_page_3,
            R.string.narration_page_4, R.string.narration_page_5,
            R.string.narration_page_6, R.string.narration_page_7,
            BookResources.PAGE_WITHOUT_NARRATION };

    public static final int LAYOUTS_LENGTH = BOOK_PAGES_LAYOUT.length;

    public static final int AUDIOS_LENGTH = audiosPath.length;
}
