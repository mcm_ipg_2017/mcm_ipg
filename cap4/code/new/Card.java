public class Card {

    private OnCheckStateListener onCheckStateListener;

    public int number, index;

    private boolean ignoreCheckStateCard = false;

    private ImageView front, back;

    private Bitmap backgroundImageBack;

    public ViewAnimator viewAnimator;

    private OnClickListener openClickListener;

    private VibrationFeedback vibrationFeedback;

    private ViewGroup VIEW_GROUP_EMPTY;

    public static boolean blockedCards = false;

    public Card (@NonNull final Context context, LayoutInflater layoutInflater,
                 final int back_card_res_image_id, final int index,
                 final OnCheckStateListener onCheckStateListener) {

        ( ... )

        viewAnimator = (ViewAnimator) LayoutInflater.from(context).
										inflate(R.layout.card_cell, VIEW_GROUP_EMPTY);
																			
        if (viewAnimator != null) {

            viewAnimator.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick (final View view) {
                    AnimationFactory.flipTransition(viewAnimator, FlipDirection.LEFT_RIGHT);
                }
            });

            createListener();

            front = (ImageView) viewAnimator.findViewById(R.id.front);
            if (front != null) {
                front.setOnClickListener(openClickListener);
            }

            back = (ImageView) viewAnimator.findViewById(R.id.back);

            ( ... )
        }
    }

    private void createListener () {
        openClickListener = new OnClickListener() {
            @Override
            public void onClick (final View view) {
                if (!blockedCards) {

                    AnimationFactory.flipTransition(viewAnimator, FlipDirection.LEFT_RIGHT);

                    if (!ignoreCheckStateCard) {
                        onCheckStateListener.onCheckState(index);
                    }
                }
            }
        };
    }

    public void close () { ... }

    public void disableClick () { ... }

    public void enableClick () { ... }

    public void cleanAll () { ... }

    public void open_card () { ... }

    public void close_card () { ... }

    private void deallocate () { ... }
}