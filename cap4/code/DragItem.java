package toymobi.eduardo.dragdropgame;

import ...

public class DragItem {

    public static final CharSequence CORRECT = "correct";
    
    public static final CharSequence INCORRECT = "incorrect";

    public static final int LETTER_1 = 0;
    public static final int LETTER_2 = 1;
    public static final int LETTER_3 = 2;

    private static final int MAX_LETTER_SPOT = 3;

    public CharSequence name;

    public Drawable image;

    public int lengthName;

    public int indexGoalSpotUp = 0;

    public int indexGoalSpotDown = 0;

    private static Random random;

    public SparseArrayCompat<ItemLetter> itemsLetter;

    public int letterImageCorrect = 0, letterImageIncorrectCorrect1 = 0,
            letterImageIncorrectCorrect2 = 0;

    public DragItem (final Resources resources, final int nameId,
            final int imageId) {

        if (resources != null) {

            name = resources.getText(nameId);

            image = resources.getDrawable(imageId);

            if (name != null) {

                lengthName = name.length();

                if (itemsLetter == null) {
                    itemsLetter = new SparseArrayCompat<ItemLetter>(lengthName);
                }

                for (int i = 0; i < lengthName; i++) {

                    final char charLetter = name.charAt(i);

                    final int imageLetterId = AlphabetStruct.getImageLetter(
                            charLetter, resources);

                    final ItemLetter item = new ItemLetter(charLetter,
                            imageLetterId);

                    if (item != null) {
                        itemsLetter.put(i, item);
                    }
                }
                init();
            }
        }
    }

    public final void init () {
        if (random == null) {
            random = new Random();
        }

        if (lengthName > 0 && lengthName <= DragAndDropGameView.MAX_LETTER_SPOT) {

            indexGoalSpotUp = random.nextInt(lengthName);

            indexGoalSpotDown = random.nextInt(MAX_LETTER_SPOT);

            letterImageCorrect = itemsLetter.get(indexGoalSpotUp).letterImageId;

            boolean finishRandom = false;

            while (!finishRandom) {

                letterImageIncorrectCorrect1 = AlphabetStruct.alphabet_image_id[random
                        .nextInt(AlphabetStruct.alphabet_image_id.length)];

                letterImageIncorrectCorrect2 = AlphabetStruct.alphabet_image_id[random
                        .nextInt(AlphabetStruct.alphabet_image_id.length)];

                if (letterImageIncorrectCorrect1 != letterImageCorrect
                        && letterImageIncorrectCorrect2 != letterImageCorrect
                        && letterImageIncorrectCorrect1 != letterImageIncorrectCorrect2) {
                    finishRandom = true;
                }
            }
        }
    }

    public final void deallocate () {

        random = null;

        name = null;

        clearParamsLetters();

        if (itemsLetter != null) {
            itemsLetter.clear();
            itemsLetter = null;
        }

        if (image != null) {
            image = null;
        }
    }

    private final void clearParamsLetters () {
        lengthName = 0;

        indexGoalSpotUp = 0;

        indexGoalSpotDown = 0;

        letterImageCorrect = 0;

        letterImageIncorrectCorrect1 = 0;

        letterImageIncorrectCorrect2 = 0;
    }
}
