package toymobi.eduardo.puzzlegame;

import ...

public class PuzzleGridView extends ViewGroup implements View.OnTouchListener {

    private static final int ANIMATION_TIME = 150;

    private int column;

    private int view_size;

    private static final int padding = 1;

    private float lastDeltaTime = 0;

    private Handler handler = new Handler();

    private int dragged = -1;

    private int lastX = -1;

    private int lastY = -1;

    private int lastTarget = -1;

    private boolean isTouching = false;

    private List<Integer> newPositionList;

    private OnPuzzleReorderListener onRearrangeListener;

    private SimpleSoundPool sfx;

    public PuzzleGridView (final Context context, final int view_size, final int column) {
        super(context);

        this.column = column;

        this.view_size = view_size;

        newPositionList = new ArrayList<Integer>();

        setOnTouchListener(this);

        handler.removeCallbacks(updateTask);

        handler.postAtTime(updateTask, SystemClock.uptimeMillis() + 250);

        setChildrenDrawingOrderEnabled(true);

        if (sfx == null) {
            sfx = new SimpleSoundPool(context, R.raw.sfx_classic_click);
        }
    }


    private Runnable updateTask = new Runnable() {
        public void run () {
            if (lastDeltaTime != 0 && !isTouching) {
                lastDeltaTime *= .9;

                if (Math.abs(lastDeltaTime) < .25) {
                    lastDeltaTime = 0;
                }
            }
            layout(getLeft(), getTop(), getRight(), getBottom());

            handler.postDelayed(this, 25);
        }
    };

    @Override
    public void addView (final View child) {
        super.addView(child);
        newPositionList.add(-1);
    };

    @Override
    public void removeViewAt (final int index) {
        super.removeViewAt(index);
        newPositionList.remove(index);
    };

    @Override
    protected void onLayout (final boolean changed, final int l, final int t, final int r, final int b) {
        
		for (int i = 0; i < getChildCount(); i++) {
            if (i != dragged) {
                final Point xy = getCoordinateFromIndex(i);
                getChildAt(i).layout(xy.x, xy.y, xy.x + view_size, xy.y + view_size);
            }
        }
        invalidate();
    }

    @Override
    protected int getChildDrawingOrder (final int childCount, final int i) {
        if (dragged == -1) {
            return i;
        } else if (i == childCount - 1) {
            return dragged;
        } else if (i >= dragged) {
            return i + 1;
        }
        return i;
    }

    private int getIndexFromCoordinate (final int x, final int y) {
        final int col = getTargetFromCoordinate(x), row = getTargetFromCoordinate(y);
        if (col == -1 || row == -1) {
            return -1;
        }
        final int index = row * column + col;

        if (index >= getChildCount()) {
            return -1;
        }
        return index;
    }

    private int getTargetFromCoordinate (int target_coordinate) {
        target_coordinate -= padding;
        for (int i = 0; target_coordinate > 0; i++) {
            if (target_coordinate < view_size) {
                return i;
            }
            target_coordinate -= (view_size + padding);
        }
        return -1;
    }

    private int getTargetFromCoordinate (final int x, final int y) {
        if (getTargetFromCoordinate(y) == -1) {
            return -1;
        }

        final int leftPos = getIndexFromCoordinate(x - (view_size / 4), y);
        final int rightPos = getIndexFromCoordinate(x + (view_size / 4), y);

        if (leftPos == -1 && rightPos == -1) {
            return -1;
        }
        if (leftPos == rightPos) {
            return -1;
        }

        int target = -1;
        if (rightPos > -1) {
            target = rightPos;
        } else if (leftPos > -1) {
            target = leftPos + 1;
        }
        if (dragged < target) {
            return target - 1;
        }
        return target;
    }

    private Point getCoordinateFromIndex (final int index) {
        final int col = index % column;
        final int row = index / column;
        return new Point(padding + (view_size + padding) * col, padding + (view_size + padding) * row);
    }

    public boolean onTouch (final View view, final MotionEvent event) {
        final int action = event.getAction();
        switch (action & MotionEvent.ACTION_MASK) {
        case MotionEvent.ACTION_DOWN:

            if (sfx != null && GlobalSettings.soundEnable) {
                sfx.playSound(R.raw.sfx_classic_click);
            }

            lastX = (int) event.getX();
            lastY = (int) event.getY();
            isTouching = true;

            final int index = getLastIndex();
            if (index != -1) {
                dragged = index;
                animateDragged();
                return true;
            }

            break;
        case MotionEvent.ACTION_MOVE:
            final int delta = lastY - (int) event.getY();
            if (dragged != -1) {
                final int x = (int) event.getX(), y = (int) event.getY();
                final int l = x - (3 * view_size / 4), t = y- (3 * view_size / 4);
                getChildAt(dragged).layout(l, t, l + (view_size * 3 / 2), t + (view_size * 3 / 2));

                final int target = getTargetFromCoordinate(x, y);
                if (lastTarget != target) {
                    if (target != -1) {
                        animateGap(target);
                        lastTarget = target;
                    }
                }
            } else {
                layout(getLeft(), getTop(), getRight(), getBottom());
            }
            lastX = (int) event.getX();
            lastY = (int) event.getY();
            lastDeltaTime = delta;
            break;
        case MotionEvent.ACTION_UP:
            if (dragged != -1) {
                final View v = getChildAt(dragged);
                if (lastTarget != -1) {
                    reorderChildren();
                } else {
                    final Point xy = getCoordinateFromIndex(dragged);
                    v.layout(xy.x, xy.y, xy.x + view_size, xy.y + view_size);
                }
                v.clearAnimation();
                if (v instanceof ImageView) {
                    ((ImageView) v).setAlpha(255);
                }
                lastTarget = -1;
                dragged = -1;
            }
            isTouching = false;
            break;
        }
        if (dragged != -1) {
            return true;
        }
        return false;
    }

    private void animateDragged () {

        final View view = getChildAt(dragged);

        final int x = getCoordinateFromIndex(dragged).x + view_size / 2, y = getCoordinateFromIndex(dragged).y + view_size / 2;

        final int l = x - (3 * view_size / 4), t = y - (3 * view_size / 4);

        view.layout(l, t, l + (view_size * 3 / 2), t + (view_size * 3 / 2));
        
		final AnimationSet animSet = new AnimationSet(true);
		
        final ScaleAnimation scale = new ScaleAnimation(.667f, 1, .667f, 1, view_size * 3 / 4, view_size * 3 / 4);

        scale.setDuration(ANIMATION_TIME);

        final AlphaAnimation alpha = new AlphaAnimation(1, 0.5f);

        alpha.setDuration(ANIMATION_TIME);

        animSet.addAnimation(scale);
        animSet.addAnimation(alpha);
        animSet.setFillEnabled(true);
        animSet.setFillAfter(true);

        view.clearAnimation();
        view.startAnimation(animSet);
    }

    private void animateGap (final int target) {
        for (int i = 0; i < getChildCount(); i++) {
            final View v = getChildAt(i);
            if (i == dragged) {
                continue;
            }
            int newPos = i;
            if (dragged < target && i >= dragged + 1 && i <= target) {
                newPos--;
            } else if (target < dragged && i >= target && i < dragged) {
                newPos++;
            }

            int oldPos = i;
            if (newPositionList.get(i) != -1) {
                oldPos = newPositionList.get(i);
            }
            if (oldPos == newPos) {
                continue;
            }

            final Point oldXY = getCoordinateFromIndex(oldPos);
            final Point newXY = getCoordinateFromIndex(newPos);
            
			final Point oldOffset = new Point(oldXY.x - v.getLeft(), oldXY.y - v.getTop());
            final Point newOffset = new Point(newXY.x - v.getLeft(), newXY.y - v.getTop());

            final TranslateAnimation translate = new TranslateAnimation( Animation.ABSOLUTE, oldOffset.x, Animation.ABSOLUTE, newOffset.x, Animation.ABSOLUTE, oldOffset.y, Animation.ABSOLUTE, newOffset.y);
			
            translate.setDuration(ANIMATION_TIME);
            translate.setFillEnabled(true);
            translate.setFillAfter(true);
            v.clearAnimation();
            v.startAnimation(translate);

            newPositionList.set(i, newPos);
        }
    }

    private void reorderChildren () {
        if (onRearrangeListener != null) {
            onRearrangeListener.onReorder(dragged, lastTarget);
        }

        final List<View> children = new ArrayList<View>();

        for (int i = 0; i < getChildCount(); i++) {
            getChildAt(i).clearAnimation();
            children.add(getChildAt(i));
        }
        removeAllViews();
        while (dragged != lastTarget) {
            if (lastTarget == children.size()) {
                children.add(children.remove(dragged));
                dragged = lastTarget;
            } else if (dragged < lastTarget) {
                Collections.swap(children, dragged, dragged + 1);
                dragged++;
            } else if (dragged > lastTarget) {
                Collections.swap(children, dragged, dragged - 1);
                dragged--;
            }
        }
        for (int i = 0; i < children.size(); i++) {
            newPositionList.set(i, -1);
            addView(children.get(i));
        }
        layout(getLeft(), getTop(), getRight(), getBottom());
    }

    private int getLastIndex () {
        return getIndexFromCoordinate(lastX, lastY);
    }

    public void setOnRearrangeListener (final OnPuzzleReorderListener l) {
        this.onRearrangeListener = l;
    }

    public final void deallocate () {

        if (handler != null && updateTask != null) {
            handler.removeCallbacks(updateTask);
            updateTask = null;
            handler = null;
        }

        if (newPositionList != null) {
            newPositionList.clear();
            newPositionList = null;
        }
    }
}