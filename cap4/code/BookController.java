package com.toymobi.book;

import ...


public class BookController {

    private ViewPager viewPager;

    private ReducedCustomPagerAdapter adapter;

    private int pageNumber;

    public CharSequence pageNumberText;

    private Context context;

    private SparseArrayCompat<CharSequence> pageText = null;

    private SparseArrayCompat<Drawable> pageImages = null;

    private SparseArrayCompat<MediaPlayerManager> pageNarration = null;

    public MenuItem menuItemPageNumber;

    public static final String PERSISTENCE_KEY_PAGE = "PERSISTENCE_KEY_PAGE";

    private LayoutInflater layoutInflater;

    public BookController(final Context context) {
        
        if (context != null) {

            this.layoutInflater = LayoutInflater.from(context);

            this.context = context;

            start();
        }
    }

    private final void createAdapter(final LayoutInflater layoutInflater) {
        if (context != null && adapter == null && layoutInflater != null) {
            adapter = new ReducedCustomPagerAdapter(layoutInflater);
            final int size = BookResources.LAYOUTS_LENGTH;
            for (int i = 0; i < size; i++) {
                adapter.addViewById(BookResources.BOOK_PAGES_LAYOUT[i], i);
            }
        }
    }

    private final void createViewPager() {
        if (adapter != null) {
            viewPager.setAdapter(adapter);

            viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(final int page) {
                    stopNarration();
                    pageNumber = page;

                    pageNumberText = "" + (pageNumber + 1);

                    if (menuItemPageNumber != null) {
                        menuItemPageNumber.setTitle(pageNumberText);
                    }
                    playNarration();
                }

                @Override
                public void onPageScrolled(final int page,
                                           final float positionOffset,
                                           final int positionOffsetPixels) {
                }

                @Override
                public void onPageScrollStateChanged(final int position) {
                }
            });
        }
    }

    public final void startBookText(final View view) {
        if (view != null) {
            viewPager = (ViewPager) view;
        }
        createViewPager();
    }

    private final void goBookPage(final int page) {
        
		stopNarration();
		
        if (viewPager != null && pageNumber < BookResources.LAYOUTS_LENGTH && page < BookResources.LAYOUTS_LENGTH) {
            pageNumber = page;
            viewPager.setCurrentItem(pageNumber);
            playNarration();
        }
    }

    public final void bookPagePreviews() {
        if (viewPager != null) {
            stopNarration();
            if (pageNumber > 0) {
                --pageNumber;
                viewPager.setCurrentItem(pageNumber);
                playNarration();
            } else {
                pageNumber = BookResources.BOOK_PAGES_SIZE - 1;
                goBookPage(pageNumber);
            }
        }
    }

    public final void bookPageNext() {
        if (viewPager != null) {
            if (pageNumber < BookResources.LAYOUTS_LENGTH - 1) {
                stopNarration();
                ++pageNumber;
                viewPager.setCurrentItem(pageNumber);
                playNarration();
            } else {
                pageNumber = 0;
                goBookPage(pageNumber);
            }
        }
    }

    private final void createNarration() {

        if (pageNarration == null) {

            final int size = BookResources.AUDIOS_LENGTH;

            pageNarration = new SparseArrayCompat<MediaPlayerManager>(size);

            for (int i = 0; i < size; i++) {
                if (BookResources.audiosPath[i] == BookResources.PAGE_WITHOUT_NARRATION) {
                    pageNarration.put(i, null);
                } else {
                    final MediaPlayerManager mediaPlayerManager = getSoundMedia(BookResources.audiosPath[i]);
                    if (mediaPlayerManager != null) {
                        pageNarration.put(i, mediaPlayerManager);
                    }
                }
            }
        }
    }

    public final void pauseNarration() {
        if (pageNarration != null) {
            if (pageNumber >= 0 && pageNumber < pageNarration.size()) {
                final MediaPlayerManager mp = pageNarration.get(pageNumber);
                if (mp != null) {
                    mp.pauseMedia();
                }
            }
        }
    }

    public final void playNarration() {
        if (GlobalSettings.soundEnable && pageNarration != null) {
            if (pageNumber >= 0 && pageNumber < pageNarration.size()) {

                final MediaPlayerManager mp = pageNarration.get(pageNumber);
                if (mp != null) {
                    if (!mp.isPlay()) {
                        mp.resetMedia(false);
                    }
                }
            }
        }
    }

    public final void resumeNarration() {
        if (GlobalSettings.soundEnable && pageNarration != null) {
            if (pageNumber >= 0 && pageNumber < pageNarration.size()) {
                final MediaPlayerManager mp = pageNarration.get(pageNumber);
                if (mp != null) {
                    mp.resumeMedia();
                }
            }
        }
    }

    public final void stopNarration() {
        if (pageNarration != null) {
            if (pageNumber >= 0 && pageNumber < pageNarration.size()) {
                final MediaPlayerManager mp = pageNarration.get(pageNumber);
                if (mp != null) {
                    mp.stopMedia();
                }
            }
        }
    }

    public final MediaPlayerManager getSoundMedia(final int resIdSoundMedia) {

        MediaPlayerManager mediaPlayerManager = null;

        if (context != null) {
            final String musicPath = context.getString(resIdSoundMedia);

            if (musicPath != null && musicPath.length() > 0) {

                mediaPlayerManager = new MediaPlayerManager(musicPath, context.getApplicationContext());

                if (mediaPlayerManager != null) {
                    mediaPlayerManager.playMedia(false, false);
                }
            }
        }
        return mediaPlayerManager;
    }

    private final void createPageImage() {

        if (context != null) {

            final int size = BookResources.BOOK_PAGES_IMAGE.length;

            if (pageImages == null) {

                pageImages = new SparseArrayCompat<Drawable>(size);

                final Resources resources = context.getResources();

                if (resources != null) {
                    for (int i = 0; i < size; i++) {

                        final int pageIndexID = BookResources.BOOK_PAGES_IMAGE[i];

                        if (pageIndexID != -1) {

                            final Drawable imageDrawable = resources
                                    .getDrawable(pageIndexID);

                            if (imageDrawable != null) {
                                pageImages.put(i, imageDrawable);
                            } else {
                                pageImages.put(i, null);
                            }

                        } else {
                            pageImages.put(i, null);
                        }
                    }
                }
            }
        }
    }

    private final void createPageImageLow() {

        if (context != null) {

            final int size = BookResources.BOOK_PAGES_IMAGE.length;

            final int height = DeviceDimensionsUtil.getDisplayHeight(context) / 2;

            final int width = height + (height / 4);

            if (pageImages == null) {

                pageImages = new SparseArrayCompat<Drawable>(size);

                final Resources resources = context.getResources();

                if (resources != null) {

                    BitmapDrawable bkg_image_temp = null;

                    for (int i = 0; i < size; i++) {

                        final int pageIndexID = BookResources.BOOK_PAGES_IMAGE[i];

                        if (pageIndexID != -1) {

                            bkg_image_temp = new BitmapDrawable(resources, UIComponentUtils.decodeSampledBitmapFromResource(resources, pageIndexID, width, height));

                            pageImages.put(i, bkg_image_temp);

                        } else {
                            pageImages.put(i, null);
                        }
                    }
                }
            }
        }
    }

    private final void createPageText() {
        if (pageText == null) {

            final int pageLength = BookResources.BOOK_PAGES_TEXT.length;

            pageText = new SparseArrayCompat<CharSequence>(pageLength);

            final int size = BookResources.BOOK_PAGES_TEXT.length;

            for (int i = 0; i < size; i++) {

                final int pageIndexID = BookResources.BOOK_PAGES_TEXT[i];

                final CharSequence page = GetTextRawFile.getTextRawFile(context.getResources(), pageIndexID);

                if (page != null) {
                    pageText.put(i, page);
                }
            }
        }
    }

    private final void loadPageText() {
        if (adapter != null && pageText != null && pageText.size() > 0) {

            for (int index = 0; index < BookResources.BOOK_PAGES_LAYOUT.length; index++) {

                final View viewTemp = adapter.getViewByIndex(index);

                if (viewTemp != null) {

                    final JustifyTextViewCustomFont textViewPageText = (JustifyTextViewCustomFont) viewTemp.findViewById(R.id.book_page_layout_text);

                    if (textViewPageText != null && index < pageText.size()) {

                        final CharSequence text = pageText.get(index);

                        if (text != null && text.length() > 0) {
                            textViewPageText.setTextJustify(text);
                        }
                    }
                }
            }
        }
    }

    private final void loadPageImage() {

        if (adapter != null && pageImages != null && pageImages.size() > 0) {

            for (int index = 0; index < BookResources.BOOK_PAGES_LAYOUT.length; index++) {

                final View viewTemp = adapter.getViewByIndex(index);

                if (viewTemp != null) {

                    final ImageView image = (ImageView) viewTemp
                            .findViewById(R.id.book_page_layout_image);

                    if (image != null) {

                        final int pageImageSize = pageImages.size();

                        if (pageImageSize > index) {
                            final Drawable imageDrawable = pageImages
                                    .get(index);

                            if (imageDrawable != null) {
                                image.setImageDrawable(imageDrawable);
                            }
                        }
                    }
                }
            }
        }
    }

    private final void start() {

        createAdapter(layoutInflater);

        if (EduardoStuff.isNormalSize(context)) {
            createPageImage();
        } else {
            createPageImageLow();
        }

        createNarration();

        createPageText();

        loadPageImage();

        loadPageText();

    }

    public final void changePageNumberText() {
        if (menuItemPageNumber != null) {
            if (pageNumber == BookResources.BOOK_PAGES_LAYOUT.length) {
                pageNumberText = "" + BookResources.BOOK_PAGES_LAYOUT.length;
            } else {
                pageNumberText = "" + (pageNumber + 1);
            }

            menuItemPageNumber.setTitle(pageNumberText);
        }
    }

    public final void clearAll() {

        if (pageText != null && pageText.size() > 0) {

            final int size = pageText.size();

            for (int i = 0; i < size; i++) {
                CharSequence text = pageText.get(i);
                if (text != null) {
                    text = null;
                }
            }
            pageText.clear();
        }

        if (adapter != null) {
            for (int index = 0; index < BookResources.BOOK_PAGES_LAYOUT.length; index++) {

                View viewTemp = adapter.getViewByIndex(index);

                if (viewTemp != null) {

                    ImageView picture = (ImageView) viewTemp
                            .findViewById(R.id.book_page_layout_image);

                    if (picture != null) {
                        picture.setImageDrawable(null);
                        picture.setImageBitmap(null);
                        picture = null;
                    }
                    viewTemp.destroyDrawingCache();
                    viewTemp = null;
                }
            }
            adapter.deallocate();
            adapter = null;
        }

        if (viewPager != null) {
            viewPager.destroyDrawingCache();
            viewPager.removeAllViews();
            viewPager.removeAllViewsInLayout();
            viewPager = null;
        }

        if (pageImages != null) {

            final int sizeImages = pageImages.size();

            for (int i = 0; i < sizeImages; i++) {
                Drawable imageDrawable = pageImages.get(i);

                if (imageDrawable != null) {
                    imageDrawable = null;
                }
            }
            pageImages.clear();
            pageImages = null;

            layoutInflater = null;
        }

        if (pageNarration != null && pageNarration.size() > 0) {

            final int size = pageNarration.size();

            for (int i = 0; i < size; i++) {
                MediaPlayerManager mediaPlayerManager = pageNarration.get(i);
                if (mediaPlayerManager != null) {
                    mediaPlayerManager.releaseMedia();
                    mediaPlayerManager = null;
                }
            }
            pageNarration.clear();
        }
    }

    public final void savePage() {
        if (EduardoStuff.ENABLE_SAVE_PAGE_BOOK) {
            PersistenceManager.saveSharedPreferencesInt(context, PERSISTENCE_KEY_PAGE, pageNumber);
        }
    }

    public final void loadPage() {

        if (EduardoStuff.ENABLE_SAVE_PAGE_BOOK) {
            pageNumber = PersistenceManager.loadSharedPreferencesInt(context, PERSISTENCE_KEY_PAGE, 0);
        }

        goBookPage(pageNumber);
    }
}
