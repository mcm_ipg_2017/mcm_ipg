package toymobi.book1.main;

import ...

public class MainMenuFragment extends Fragment {

    private SimpleSoundPool sfx;

    private VibrationFeedback vibrationFeedback;

    private MusicBackgroundController musicBackgroundController;

    private static final int[] options = { R.id.option_1, R.id.option_2,
            R.id.option_3, R.id.option_4, R.id.option_5, R.id.option_6,
            R.id.option_7 };

    public static final String MAIN_MENU_FRAGMENT_TAG = "MAIN_MENU_FRAGMENT";

    private OnClickListener optionOnClickListener;

    private CharSequence pageInfoText = null;

    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        createActionBar();
    }

    @Override
    public final View onCreateView (final LayoutInflater inflater,
            final ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.menu_layout, container,
                false);

        createActionBar();

        EduardoStuff.isMainMenu = true;

        createStuff(view);

        return view;
    }

    private void createStuff (final View view) {

        if (vibrationFeedback == null) {
            vibrationFeedback = new VibrationFeedback(getActivity());
        }

        if (sfx == null) {
            sfx = new SimpleSoundPool(getActivity(), R.raw.sfx_normal_click);
        }

        if (musicBackgroundController == null) {
            musicBackgroundController = new MusicBackgroundController(
                    getActivity(), R.string.path_sound_game_menu);
        }

        if (optionOnClickListener == null) {

            optionOnClickListener = new OnClickListener() {

                @Override
                public void onClick (final View view) {
                    if (view != null) {
                        final int viewID = view.getId();

                        switch (viewID) {
                        case R.id.option_1:
                            goOption1();
                            break;
                        case R.id.option_2:
                            goOption2();
                            break;
                        case R.id.option_3:
                            goOption3();
                            break;
                        case R.id.option_4:
                            goOption4();
                            break;
                        case R.id.option_5:
                            goToOption5();
                            break;
                        case R.id.option_6:
                            goToOption6();
                            break;
                        case R.id.option_7:
                            goToOption7();
                            break;
                        }
                    }
                }
            };
        }

        for (int i = 0; i < options.length; i++) {

            final View optionView = view.findViewById(options[i]);

            if (optionView != null && optionOnClickListener != null) {
                optionView.setOnClickListener(optionOnClickListener);
            }
        }
    }

    @Override
    public final void onCreateOptionsMenu (final Menu menu,
            final MenuInflater inflater) {
        if (menu != null) {
            menu.clear();

            if (inflater != null) {
                inflater.inflate(R.menu.main_menu, menu);

                final MenuItem itemSound = menu.findItem(R.id.sound_main_menu);

                if (itemSound != null) {
                    if (GlobalSettings.soundEnable) {
                        itemSound.setIcon(R.drawable.icon_sound_on);
                    } else {
                        itemSound.setIcon(R.drawable.icon_sound_off);
                    }
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected (final MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
        case R.id.info_main_menu:
            goToInfo();
            return true;
        case R.id.sound_main_menu:

            GlobalSettings.soundEnable ^= true;

            if (GlobalSettings.soundEnable) {

                item.setIcon(R.drawable.icon_sound_on);

                if (musicBackgroundController != null) {
                    musicBackgroundController.resumeMusic();
                }

            } else {

                item.setIcon(R.drawable.icon_sound_off);

                if (musicBackgroundController != null) {
                    musicBackgroundController.pauseMusic();
                }
            }
            return true;

        case R.id.exit_menu:
            EduardoStuff.showMessageExitSPC(getActivity());
            return true;

        default:
            return super.onOptionsItemSelected(item);
        }
    }

    private void goToInfo () {

        if (pageInfoText == null) {
            pageInfoText = GetTextRawFile.getTextRawFile(getResources(),
                    R.raw.info_app);
        }

        if (pageInfoText != null && pageInfoText.length() > 0) {
            EduardoStuff.showInfo(getActivity(), pageInfoText);
        }
    }

    @Override
    public final void onResume () {
        super.onResume();

        EduardoStuff.setLanguage(getActivity());

        if (musicBackgroundController != null) {
            musicBackgroundController.playMusic();
        }
    }

    @Override
    public final void onPause () {
        super.onPause();

        if (musicBackgroundController != null) {
            musicBackgroundController.stopMusic();
        }
    }

    @Override
    public final void onStop () {
        super.onStop();

        if (musicBackgroundController != null) {
            musicBackgroundController.stopMusic();
        }
    }

    @Override
    public final void onDestroy () {
        super.onDestroy();
        deallocate();
    }

    private void createActionBar () {

        setHasOptionsMenu(true);

        final ActionBar actionBar = ((ActionBarActivity) getActivity())
                .getSupportActionBar();

        if (actionBar != null) {

            actionBar.setDisplayHomeAsUpEnabled(false);

            final SpannableString s = new SpannableString(
                    getText(R.string.app_name));

            s.setSpan(new TypefaceSpan("Roboto-Black.ttf"), 0, s.length(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            actionBar.setTitle(s);

            final int titleId = Resources.getSystem().getIdentifier(
                    "action_bar_title", "id", "android");

            if (titleId > 0) {

                final TextView title = (TextView) ((ActionBarActivity) getActivity())
                        .findViewById(titleId);

                if (title != null) {
                    title.setTextAppearance(
                            ((ActionBarActivity) getActivity()),
                            R.style.TextTitleActionBar);
                }
            }
            final int colorActionBar = getResources().getColor(
                    R.color.ACTION_BAR_COLOR);

            actionBar.setBackgroundDrawable(new ColorDrawable(colorActionBar));
        }
    }

    public final void playFeedbackButton () {
        if (vibrationFeedback != null) {
            vibrationFeedback.vibrate();
        }

        if (sfx != null && GlobalSettings.soundEnable) {
            sfx.playSound(R.raw.sfx_normal_click);
        }
    }

    private void goOption1 () {

        EduardoStuff.isMainMenu = false;

        final FragmentManager fragmentManager = getActivity()
                .getSupportFragmentManager();

        if (fragmentManager != null) {
            final FragmentTransaction fragmentTransaction = fragmentManager
                    .beginTransaction();

            if (fragmentTransaction != null) {

                BookFrag bookFrag = (BookFrag) fragmentManager
                        .findFragmentByTag(BookFrag.FRAGMENT_TAG_BOOK_TEXT);

                if (bookFrag == null) {

                    bookFrag = new BookFrag();

                    fragmentTransaction.setCustomAnimations(
                            android.R.anim.fade_in, android.R.anim.fade_out,
                            android.R.anim.fade_in, android.R.anim.fade_out);

                    fragmentTransaction.add(
                            R.id.main_layout_container_fragment, bookFrag,
                            BookFrag.FRAGMENT_TAG_BOOK_TEXT);

                    fragmentTransaction.addToBackStack(null);

                    fragmentTransaction.commit();

                    fragmentTransaction.remove(this);

                    deallocate();
                } else {

                    fragmentTransaction.setCustomAnimations(
                            android.R.anim.fade_in, android.R.anim.fade_out,
                            android.R.anim.fade_in, android.R.anim.fade_out);

                    fragmentTransaction.replace(
                            R.id.main_layout_container_fragment, bookFrag,
                            BookFrag.FRAGMENT_TAG_BOOK_TEXT);

                    fragmentTransaction.addToBackStack(null);

                    fragmentTransaction.commit();

                    fragmentTransaction.remove(this);

                    deallocate();
                }
            }
        }
    }

    ...

    private void deallocate () {
        if (sfx != null) {
            sfx.release();
            sfx = null;
        }

        if (vibrationFeedback != null) {
            vibrationFeedback = null;
        }

        if (musicBackgroundController != null) {
            musicBackgroundController.stopMusic();
            musicBackgroundController = null;
        }

        if (pageInfoText != null) {
            pageInfoText = null;
        }
    }

}