public class LockableHorizontalScrollView extends HorizontalScrollView {

    public LockableHorizontalScrollView (final Context context)  { ... }

    @Override
    public boolean onTouchEvent (final MotionEvent motionEvent) {
		
        // correcao no scroll na vertical e horizontal
        final int action = motionEvent.getAction();
        
		if (action == MotionEvent.ACTION_MOVE && getParent() != null) {
            getParent().requestDisallowInterceptTouchEvent(true);
        }
        return super.onTouchEvent(motionEvent);
    }

    @Override
    public boolean onInterceptTouchEvent (MotionEvent motionEvent) {
        
		if (getParent() != null) {
        
			// quando estiver movendo na horizontal bloqueia o scroll na vertical
            switch (motionEvent.getAction()) {
                
				case MotionEvent.ACTION_MOVE:
                    getParent().requestDisallowInterceptTouchEvent(true);
                    break;
            
				case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    getParent().requestDisallowInterceptTouchEvent(false);
                    break;
            }
        }
        return super.onInterceptTouchEvent(motionEvent);
    }
}