public class VibrationFeedback {
	
    private Vibrator vibrator;	
    
	private static final long VIBRATE_MILLISECONDS_DEFAULT = 100;

    public VibrationFeedback(final Context context) {
        if (context != null) {
            if (vibrator == null) {
                if (checkVibratePermission(context)) {
                    vibrator = (Vibrator) context
                            .getSystemService(Context.VIBRATOR_SERVICE);
                } 
            }
        }
    }

    private final boolean checkVibratePermission(final Context context) {
        boolean result = false;
        if (context != null) {
            final String permission = "android.permission.VIBRATE";
            final int res = context.checkCallingOrSelfPermission(permission);
            result = (res == PackageManager.PERMISSION_GRANTED);
        }
        return result;
    }

    public final void vibrate(final long milliseconds) {
        if (vibrator != null && milliseconds > 0) {
            vibrator.vibrate(milliseconds);
        }
    }

    public final void vibrate() {
        if (vibrator != null) {
            vibrator.vibrate(VIBRATE_MILLISECONDS_DEFAULT);
        }
    }
}