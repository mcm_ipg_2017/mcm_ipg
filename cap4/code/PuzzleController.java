package toymobi.eduardo.puzzlegame;

import ...

public class PuzzleController {

    private Random random;

    private PuzzleGridView puzzleGridView;

    public int width;

    public int height;

    public int view_size;

    public int index_image = 0;

    private Context context;

    private final List<CharSequence> new_goal = new ArrayList<CharSequence>(
            PuzzleValues.SIZE);

    private final List<CharSequence> poem = new ArrayList<CharSequence>(
            PuzzleValues.SIZE);

    private final List<CharSequence> result_ok = new ArrayList<CharSequence>(
            PuzzleValues.SIZE);

    private SparseArrayCompat<BitmapDrawable> imagesList;

    private SparseArrayCompat<Bitmap> piecesImageList;

    private ViewGroup container;

    private int textSize, textColor;

    public MenuItem menuItemChangePicture;

    private SimpleSoundPool sfx;

    public PuzzleController (final Context context,
            final ViewGroup layout_container) {
        if (context != null && layout_container != null) {

            this.context = context;

            this.container = layout_container;

            int widthTemp = DeviceDimensionsUtil.getDisplayWidth(context);

            if (!EduardoStuff.isNormalSize(context)) {

                final int h = DeviceDimensionsUtil.getDisplayHeight(context);

                widthTemp = h + (h / 4);
            }

            view_size = widthTemp / PuzzleValues.COLUMN;

            width = view_size * PuzzleValues.COLUMN;

            height = view_size * PuzzleValues.LINE;

            index_image = 0;

            textSize = UIComponentUtils.getDimensionPixelSizeByTheme(
                    context.getTheme(), R.dimen.text_med, 25);

            textColor = context.getResources().getColor(R.color.SNOW);

            loadSfx();

            start();
        }
    }

    private void start () {
        clean();

        loadBkgImages();

        createBkgImages();

        addDraggableGridView();

        setListeners();

        addPuzzle();
    }

    private void loadBkgImages () {

        if (context != null) {
            final Resources resources = context.getResources();

            if (resources != null) {

                final int imageLength = EduardoStuff.isNormalSize(context) ? PuzzleValues.IMAGES_RES_ID.length
                        : PuzzleValues.IMAGES_RES_ID_SMALL.length;

                if (imagesList == null) {
                    imagesList = new SparseArrayCompat<BitmapDrawable>(
                            imageLength);

                    BitmapDrawable bkg_image_temp = null;

                    for (int i = 0; i < imageLength; i++) {

                        bkg_image_temp = new BitmapDrawable(
                                resources,
                                UIComponentUtils.decodeSampledBitmapFromResource(
                                        resources,
                                        EduardoStuff.isNormalSize(context) ? PuzzleValues.IMAGES_RES_ID[i]
                                                : PuzzleValues.IMAGES_RES_ID_SMALL[i],
                                        width, height));

                        imagesList.put(i, bkg_image_temp);
                        bkg_image_temp = null;
                    }
                }
            }
        }
    }

    private void createBkgImages () {

        if (piecesImageList == null) {
            piecesImageList = new SparseArrayCompat<Bitmap>(PuzzleValues.SIZE);
        }

        Bitmap croc_bitmap = null;

        Bitmap bitmapTemp = imagesList.get(index_image).getBitmap();

        for (int i = 0; i < PuzzleValues.SIZE; i++) {

            final int line = i / PuzzleValues.COLUMN;

            final int column = ((i < PuzzleValues.COLUMN) ? i
                    : (i % (line * PuzzleValues.COLUMN)));

            final int x = view_size * column;

            final int y = view_size * line;

            if (imagesList != null) {

                croc_bitmap = Bitmap.createBitmap(bitmapTemp, x, y, view_size,
                        view_size);

                if (croc_bitmap != null) {

                    piecesImageList.put(i, croc_bitmap);

                    croc_bitmap = null;
                }
            }
        }
    }

    private void addPuzzle () {

        for (int i = 0; i < PuzzleValues.SIZE; i++) {
            new_goal.add(String.valueOf(i));
        }

        if (random == null) {
            random = new Random();
        }

        for (int i = 0; i < PuzzleValues.SIZE; i++) {
            final CharSequence word = new_goal.remove(random.nextInt(new_goal
                    .size()));

            final ImageView view = new ImageView(this.context);

            view.setImageBitmap(drawTextToBitmap(word));

            // view.setSelected(false);

            view.setDrawingCacheEnabled(true);

            view.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

            puzzleGridView.addView(view);

            poem.add(word);

            result_ok.add(String.valueOf(i));
        }
    }

    private void setListeners () {
        puzzleGridView.setOnRearrangeListener(new OnPuzzleReorderListener() {

            @Override
            public void onReorder (final int old_index, final int new_index) {
                final CharSequence word = poem.remove(old_index);
                poem.add(new_index, word);
                for (int i = 0; i < PuzzleValues.SIZE; i++) {
                    final CharSequence value = poem.get(i);
                    final CharSequence result = result_ok.get(i);

                    if (value != result) {
                        i = PuzzleValues.SIZE + 1;
                    } else if (i == PuzzleValues.SIZE - 1) {
                        win();
                    }
                }

            }
        });
    }

    private void addDraggableGridView () {

        if (puzzleGridView != null) {
            puzzleGridView.deallocate();

            puzzleGridView = null;
        }

        puzzleGridView = new PuzzleGridView(this.context, view_size,
                PuzzleValues.COLUMN);

        container.addView(puzzleGridView);
    }

    private void win () {
        if (GlobalSettings.soundEnable) {
            sfx.playSound(R.raw.sfx_win);
        }

        showMessageWinner();
    }

    private Bitmap drawTextToBitmap (final CharSequence textNumber) {
        final float scale = context.getResources().getDisplayMetrics().density;

        final int index = Integer.parseInt(textNumber.toString());

        Bitmap bitmap = piecesImageList.get(index);

        Config bitmapConfig = bitmap.getConfig();

        if (bitmapConfig == null) {
            bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
        }

        bitmap = bitmap.copy(bitmapConfig, true);

        final Canvas canvas = new Canvas(bitmap);

        final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(textColor);
        paint.setTextSize((int) (textSize * scale));
        paint.setTypeface(Typeface.DEFAULT_BOLD);

        final Rect bounds = new Rect();
        paint.setTextAlign(Align.CENTER);

        paint.getTextBounds(textNumber.toString(), 0, textNumber.length(),
                bounds);

        canvas.drawText(textNumber.toString(), view_size >> 1, view_size >> 1,
                paint);

        RectF rect = new RectF(0.0f, 0.0f, view_size, view_size);

        paint.setStyle(Paint.Style.STROKE);

        canvas.drawRoundRect(rect, 0, 0, paint);

        return bitmap;
    }

    private void clean () {
        if (piecesImageList != null) {
            piecesImageList.clear();
        }
    }

    public void deallocateAll () {
        if (puzzleGridView != null) {
            puzzleGridView.removeAllViews();

            puzzleGridView.deallocate();

            puzzleGridView = null;
        }

        clean();

        if (piecesImageList != null && piecesImageList.size() > 0) {
            piecesImageList.clear();
        }

        if (new_goal != null) {
            new_goal.clear();
        }

        if (poem != null) {
            poem.clear();
        }

        if (result_ok != null) {
            result_ok.clear();
        }

        random = null;

        if (sfx != null) {
            sfx.release();
            sfx = null;
        }
    }

    private void showMessageWinner () {
        if (context != null) {

            final OnClickListener okConfirmation = new OnClickListener() {
                @Override
                public void onClick (final View view) {
                    changeLevel();
                }
            };

            final CharSequence title = context
                    .getText(R.string.congrats_message);

            if (title != null) {
                EduardoStuff.showCongratsDialog(context, title, okConfirmation);
            }
        }
    }

    private void loadSfx () {
        if (context != null) {
            if (sfx == null) {
                sfx = new SimpleSoundPool(context, R.raw.sfx_win);
            }
        }
    }

    public final void changeLevel () {
        if (container != null) {
            ((ViewGroup) container).removeView(puzzleGridView);

            new_goal.clear();

            poem.clear();

            result_ok.clear();

            final int size = EduardoStuff.isNormalSize(context) ? PuzzleValues.BACKGROUND_SIZE - 1 : PuzzleValues.BACKGROUND_SIZE_SMALL - 1;

            if (index_image < size) {
                index_image++;
            } else {
                index_image = 0;
            }
            start();
        }
    }
}
