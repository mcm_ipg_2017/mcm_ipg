// Setar o objeto TTS nos pictogramas dentro da classe Controller
public final void setTTS () {
	if (imageViewSPC != null && imageViewSPC.size() > 0) {

		final int length = imageViewSPC.size();

		for (int i = 0; i < length; i++) {
			final ImageViewAnimationTTS imageViewAnimationTTS = imageViewSPC.get(i);

			if (imageViewAnimationTTS != null) {
				imageViewAnimationTTS.setTextToSpeech(textToSpeech);
			}
		}
	}
}