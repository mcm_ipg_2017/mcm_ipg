public class EduardoStuff {

    public static boolean isMainMenu = true;

    public static final String PERSISTENCE_KEY = "MACARENA";

    public static boolean ENABLE_TTS = true;

    public static boolean ENABLE_SAVE_PAGE_BOOK = false;

    public static Random random = new Random();

    public static final void showMessageExitSPC(final Context context) { ... }

    public static final void showMessageBackMainMenuSPC(final Context context) { ... }

    private static final void exit(final Context context) { ... }

    private static final void goHome(final Context context) { ... }


    public static final void showCongratsDialog(final Context context,
                                                final CharSequence infoText, 
												final OnClickListener ok) { ... }

    public static final void setLanguage(final Context context) { ... }

    public static final boolean isNormalSize(final Context context) { ... }
}