package toymobi.eduardo.memorygame;

import ...

public class Card {

    private OnCheckStateListener onCheckStateListener;

    public int number, index;

    private boolean ignoreCheckStateCard = false;

    private ImageView front;

    private ImageView back;

    private Bitmap backgroundImageBack;

    public ViewAnimator viewAnimator;

    private OnClickListener openClickListener;

    private VibrationFeedback vibrationFeedback;

    private final ViewGroup VIEW_GROUP_EMPTY = null;

    public static boolean blockedCards = false;

    public Card (final Context context, final LayoutInflater layoutInflater, final int back_card_res_image_id, final int index, final OnCheckStateListener onCheckStateListener) {
        if (context != null) {

            if (vibrationFeedback == null) {
                vibrationFeedback = new VibrationFeedback(context);
            }

            this.onCheckStateListener = onCheckStateListener;

            this.number = index;

            viewAnimator = (ViewAnimator) LayoutInflater.from(context).inflate(R.layout.card_cell, VIEW_GROUP_EMPTY);

            if (viewAnimator != null) {

                viewAnimator.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick (final View view) {
                        AnimationFactory.flipTransition(viewAnimator, FlipDirection.LEFT_RIGHT);
                    }
                });

                createListener();

                front = (ImageView) viewAnimator.findViewById(R.id.front);
                if (front != null) {
                    front.setOnClickListener(openClickListener);
                }

                back = (ImageView) viewAnimator.findViewById(R.id.back);
                
				if (back != null && back_card_res_image_id > 0) {
                    back.setImageResource(back_card_res_image_id);

                    if (back != null) {
                        back.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick (View view) {
                                if (view != null) {
                                    if (!blockedCards) {

                                    }
                                }
                            }
                        });
                    }
                }
            }
        }
    }

    private void createListener () {
        openClickListener = new OnClickListener() {
            @Override
            public void onClick (final View view) {

                if (!blockedCards) {

                    if (vibrationFeedback != null) {
                        vibrationFeedback.vibrate();
                    }

                    AnimationFactory.flipTransition(viewAnimator, FlipDirection.LEFT_RIGHT);

                    if (!ignoreCheckStateCard) {
                        onCheckStateListener.onCheckState(index);
                    }
                }
            }
        };
    }

    public void close () {
        ignoreCheckStateCard = true;
        if (back != null) {
            viewAnimator.performClick();
            if (front != null) {
                front.setClickable(true);
            }
            ignoreCheckStateCard = false;
        }
    }

    public void disableClick () {
        if (front != null) {
            front.setClickable(false);
        }
        if (viewAnimator != null) {
            viewAnimator.setClickable(false);
        }
    }

    public void enableClick () {
        if (front != null) {
            front.setClickable(true);
        }
        if (viewAnimator != null) {
            viewAnimator.setClickable(true);
        }
    }

    public void cleanAll () {
        deallocate();

        if (viewAnimator != null) {
            viewAnimator.removeAllViewsInLayout();
            viewAnimator.removeAllViews();
            back = null;
            viewAnimator = null;
        }
    }

    public void open_card () {
        AnimationFactory.flipTransition(viewAnimator, FlipDirection.LEFT_RIGHT);
    }

    public void close_card () {
        AnimationFactory.flipTransition(viewAnimator, FlipDirection.RIGHT_LEFT);
    }

    private final void deallocate () {

        if (back != null) {
            back.destroyDrawingCache();
            back.setImageBitmap(null);
            back = null;
        }

        if (front != null) {
            front.destroyDrawingCache();
            front.setImageBitmap(null);
            front = null;
        }

        if (backgroundImageBack != null) {
            backgroundImageBack.recycle();
            backgroundImageBack = null;
        }
    }
}