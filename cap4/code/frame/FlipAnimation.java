package com.toymobi.framework.flip3d;

import android.view.animation.Animation;
import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.animation.Transformation;

public class FlipAnimation extends Animation {
    private final float mFromDegrees;
    private final float mToDegrees;
    private final float mCenterX;
    private final float mCenterY;
    private Camera mCamera;

    private final ScaleUpDownEnum scaleType;
    public static final float SCALE_DEFAULT = 0.75f;

    private final float scale;

    public FlipAnimation (final float fromDegrees, final float toDegrees,
						  final float centerX, final float centerY, 
						  final float scale, final ScaleUpDownEnum scaleType) {
							  
        mFromDegrees = fromDegrees;
        mToDegrees = toDegrees;
        mCenterX = centerX;
        mCenterY = centerY;
        
		this.scale = (scale <= 0 || scale >= 1) ? SCALE_DEFAULT : scale;
        
		this.scaleType = scaleType == null ? ScaleUpDownEnum.SCALE_CYCLE : scaleType;
    }

    @Override
    public void initialize (final int width, final int height,
            final int parentWidth, final int parentHeight) {
        super.initialize(width, height, parentWidth, parentHeight);
        mCamera = new Camera();
    }

    @Override
    protected void applyTransformation (final float interpolatedTime,
            final Transformation t) {
				
        final float fromDegrees = mFromDegrees;
        final float degrees = fromDegrees + ((mToDegrees - fromDegrees) * interpolatedTime);

        final float centerX = mCenterX;
        final float centerY = mCenterY;
        final Camera camera = mCamera;

        final Matrix matrix = t.getMatrix();

        camera.save();

        camera.rotateY(degrees);

        camera.getMatrix(matrix);
        
		camera.restore();

        matrix.preTranslate(-centerX, -centerY);
        matrix.postTranslate(centerX, centerY);

        matrix.preScale(scaleType.getScale(scale, interpolatedTime),
                scaleType.getScale(scale, interpolatedTime), centerX, centerY);

    }

    public static enum ScaleUpDownEnum {
        SCALE_UP, SCALE_DOWN, SCALE_CYCLE, SCALE_NONE;

        public float getScale (float max, float iter) {
            switch (this) {
            case SCALE_UP:
                return max + (1 - max) * iter;

            case SCALE_DOWN:
                return 1 - (1 - max) * iter;

            case SCALE_CYCLE: {
                final boolean halfWay = (iter > 0.5);

                if (halfWay) {
                    return max + (1 - max) * (iter - 0.5f) * 2;
                } else {
                    return 1 - (1 - max) * (iter * 2);
                }
            }

            default:
                return 1;
            }
        }
    }
}