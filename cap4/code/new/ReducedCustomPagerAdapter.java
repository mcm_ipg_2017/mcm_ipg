public class ReducedCustomPagerAdapter extends PagerAdapter {
    private LayoutInflater layoutInflater;
    private SparseArrayCompat<View> listViews;

    public ReducedCustomPagerAdapter(@NonNull LayoutInflater layoutInflater) {
        this.layoutInflater = layoutInflater;
    }

    public final void addViewById(final int idView, final int position) {
        if (idView > 0) {
            if (listViews == null) {
                listViews = new SparseArrayCompat<View>();
            }
            final View view = layoutInflater.inflate(idView, null);

            if (view != null) {
                listViews.put(position, view);
            }
        }
    }

    @Override
    public final boolean isViewFromObject(final View view, final Object object) {

        if (view != null && object != null) {
            return view == object;
        }
        return false;
    }

    @Override
    public final void destroyItem(final View view, final int index,
                                  final Object object) {

        if (view != null && index >= 0 && object != null) {
            ((ViewPager) view).removeView((View) object);
        }
    }

    @Override
    public final Object instantiateItem(final View view, final int position) {
        View viewItem = null;
        if (view != null && position >= 0) {
            viewItem = listViews.get(position);
            view.addView(viewItem);
        }
        return viewItem;
    }

    public final View getViewByIndex(final int index) {
        View view = null;
        if (listViews != null) {

            final int size = listViews.size();

            if (index >= 0 && index < size) {
                view = listViews.get(index);
            }
        }
        return view;
    }

    public final void deallocate() {
        if (listViews != null) {
            final int size = listViews.size();

            if (size > 0) {
                listViews.clear();
                listViews = null;
            }
        }
    }
}