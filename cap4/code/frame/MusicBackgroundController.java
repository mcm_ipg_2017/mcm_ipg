package com.toymobi.framework.media;

import android.content.Context;

import com.toymobi.framework.options.GlobalSettings;

public class MusicBackgroundController {

    private MediaPlayerManager music = null;

    public MusicBackgroundController(final Context context,
                                     final int musicBkgID) {

        if (context != null) {
            final String musicPath = context.getString(musicBkgID);

            if (musicPath != null && musicPath.length() > 0) {

                music = new MediaPlayerManager(musicPath,
                        context.getApplicationContext());

                if (music != null) {

                    if (GlobalSettings.soundEnable) {
                        music.playMedia(false, GlobalSettings.soundEnable);
                    } else {
                        music.pauseMedia();
                    }
                }
            }
        }
    }

    public final void playMusic() {
        if (music != null) {
            music.playMedia(true, GlobalSettings.soundEnable);
        }
    }

    public final void stopMusic() {
        if (music != null) {
            music.stopMedia();
        }

        if (music != null) {
            music.releaseMedia();
        }
    }

    public final void resumeMusic() {
        if (music != null) {
            music.resumeMedia();
        }
    }

    public final void pauseMusic() {
        if (music != null) {
            music.pauseMedia();
        }
    }
}