// Metodos adicionados na classe Fragment
private VideoView videoView;
private MediaController mediaController;

public final void createPlayer(@NonNull final View rootView) {
	
	if (videoMediaControl == null) {
		videoMediaControl = new VideoMediaControl();
		videoView = (VideoView) rootView.findViewById(R.id.videoView);

		if (videoView != null && context != null) {

			mediaController = new MediaController(context) {
				@Override
				public void hide() {
					mediaController.show();
				}

				@Override
				public void setAnchorView(final View view) { ... }
			};

			mediaController.setMediaPlayer(videoMediaControl);
			videoView.setMediaController(mediaController);
			videoView.requestFocus();
		}
		play();
	}
}
public void play() {
	if (context != null && videoView != null &&
			videosPathString != null &&
			videosPathString.size() > 0) {

		videoView.setVideoURI(Uri.parse(videosPathString.get(pageNumber)));
		videoView.start();

		if (menuItemPlay != null) {
			menuItemPlay.setIcon(R.drawable.icon_pause);
			menuItemPlay.setTitle(R.string.pause_menu);
			isPlayingVideo = true;
		}

		videoView.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(final MediaPlayer mediaPlayer) {
				
				if (mediaPlayer != null) {
					
					currentPosition = mediaPlayer.getDuration();

					if (menuItemPlay != null) {
						menuItemPlay.setIcon(R.drawable.icon_play);
						menuItemPlay.setTitle(R.string.play_menu);
						isPlayingVideo = false;
					}
				}
			}
		});
		mediaController.show(0);
	}
}

private void createVideosPath() {
	
	SparseArrayCompat videosPathString = new SparseArrayCompat<String>(BookGestualResources.BOOK_PAGES_SIZE);
	
	final String path = "android.resource://" + context.getPackageName() + "/";

	int i = 0;

	for (final int id : BookGestualResources.BOOK_PAGES) {
		videosPathString.put(i, path + id);
		i++;
	}
}
}

public final void resumeVideo() { ... }

public final void pauseVideo() { ... }

public final void stopVideo() { ... }