// Exemplo do uso da classe ReducedCustomPagerAdapter
private ReducedCustomPagerAdapter adapter;

private final void createAdapter (final LayoutInflater layoutInflater) {
	if (context != null && adapter == null && layoutInflater != null) {
		adapter = new ReducedCustomPagerAdapter(layoutInflater);
		final int size = BookSPCResources.LAYOUTS_LENGTH;
		for (int i = 0; i < size; i++) {
			adapter.addViewById(BookSPCResources.BOOK_PAGES_LAYOUT[i], i);
		}
	}
}

private final void createViewPager () {
	if (adapter != null) {
		viewPager.setAdapter(adapter);
		viewPager.setOnPageChangeListener(new OnPageChangeListener() {
			
			@Override
			public void onPageSelected (final int page) {
				pageNumber = page;
				pageNumberText = "" + (pageNumber + 1);

				if (menuItemPageNumber != null) {
					menuItemPageNumber.setTitle(pageNumberText);
				}
			}

			@Override
			public void onPageScrolled (final int page,
					final float positionOffset,
					final int positionOffsetPixels) {
			}

			@Override
			public void onPageScrollStateChanged (final int position) {
			}
		});
	}
}

