// Exemplo do uso da classe UIComponentUtils
private final void createPageImageLow() {
	if (context != null) {

		final int size = BookResources.BOOK_PAGES_IMAGE.length;

		final int height = DeviceDimensionsUtil.getDisplayHeight(context) / 2;

		final int width = height + (height / 4);

		if (pageImages == null) {

			pageImages = new SparseArrayCompat<Drawable>(size);

			final Resources resources = context.getResources();

			if (resources != null) {

				BitmapDrawable bkg_image_temp = null;

				for (int i = 0; i < size; i++) {
					final int pageIndexID = BookResources.BOOK_PAGES_IMAGE[i];

					if (pageIndexID != -1) {
						bkg_image_temp = new BitmapDrawable(resources, 
							UIComponentUtils.decodeSampledBitmapFromResource(resources, pageIndexID, 
																			 width, 
																			 height));

						pageImages.put(i, bkg_image_temp);
					} else {
						pageImages.put(i, null);
					}
				}
			}
		}
	}
}