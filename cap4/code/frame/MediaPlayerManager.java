package com.toymobi.framework.media;

import ...

public class MediaPlayerManager {

    private MediaPlayer mediaPlayer;

    private final Context context;
    private Uri mediaUri;
    private int currentPosition = 0;

    public MediaPlayerManager(final String mediaPath, final Context context) {
        this.context = context;
        mediaUri = Uri.parse(mediaPath);
    }

    public final void resetMedia(final boolean isLooping) {
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            try {
                mediaPlayer.setDataSource(context, mediaUri);
                mediaPlayer.prepareAsync();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (mediaUri != null) {
                try {
                    mediaPlayer.setLooping(isLooping);
                    mediaPlayer
                            .setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                @Override
                                public void onPrepared(MediaPlayer arg0) {
                                    mediaPlayer.start();
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public final void playMedia(final boolean isLooping, final boolean start) {
        if (mediaUri != null) {
            if (mediaPlayer == null) {
                mediaPlayer = new MediaPlayer();
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

                try {
                    mediaPlayer.setDataSource(context, mediaUri);
                    mediaPlayer.prepareAsync();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (mediaUri != null) {
                try {
                    mediaPlayer.setLooping(isLooping);

                    mediaPlayer
                            .setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                @Override
                                public void onPrepared(MediaPlayer arg0) {
                                    if (start) {
                                        mediaPlayer.start();
                                    }
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public final void pauseMedia() {
        if (mediaPlayer != null) {
            mediaPlayer.pause();
            currentPosition = mediaPlayer.getCurrentPosition();
        }
    }

    public final void resumeMedia() {
        if (mediaPlayer != null) {
            mediaPlayer.seekTo(currentPosition);
            mediaPlayer.start();
        }
    }

    public final void stopMedia() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
        }
    }

    public final void releaseMedia() {
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    public final boolean isPlay() {
        return mediaPlayer != null && mediaPlayer.isPlaying();
    }
}
