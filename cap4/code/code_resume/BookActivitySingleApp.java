public class BookActivitySingleApp extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.toymobi.book.R.layout.main_layout_book);
        startBookFrag();
    }

    private final void startBookFrag() { ... }
}
