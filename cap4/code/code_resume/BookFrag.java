public class BookFrag extends Fragment {

    private SimpleSoundPool sfx;

    private MusicBackgroundController musicBackgroundController;

    private BookController bookController;

    private VibrationFeedback vibrationFeedback;

    @Override
    public final void onCreate(final Bundle savedInstanceState) { ... }

    @Override
    public void onResume() { ... }

    @Override
    public void onPause() { ... }

    @Override
    public final void onStop() { ... }

    @Override
    public void onDestroy() { ... }

    @Override
    public final void onCreateOptionsMenu(final Menu menu, 
										  final MenuInflater inflater) { ... }
	
	@Override
	public final boolean onOptionsItemSelected(final MenuItem item) { ... }

    @Override
    public final View onCreateView(final LayoutInflater inflater, 
								   final ViewGroup container, 
								   final Bundle savedInstanceState) { 
		
		final View view = inflater.inflate(R.layout.layout_viewpager, container, false);

        if (bookController == null) {
            bookController = new BookController(getActivity());
        }
		( ... )
	}
	
    private final void createActionBar() { ... }
    
	private void createSFX() { ... }

    private void deallocate() { ... }

    public final void playFeedbackButton() { ... }
}