public class PuzzleController {

    private Random random;

    private PuzzleGridView puzzleGridView;

    public int width, height, view_size, index_image;

    private Context context;

    private final List<CharSequence> new_goal = new ArrayList<CharSequence>(
            PuzzleValues.SIZE);

    private final List<CharSequence> poem = new ArrayList<CharSequence>(
            PuzzleValues.SIZE);

    private final List<CharSequence> result_ok = new ArrayList<CharSequence>(
            PuzzleValues.SIZE);

    private SparseArrayCompat<BitmapDrawable> imagesList;

    private SparseArrayCompat<Bitmap> piecesImageList;

    private ViewGroup container;

    private int textSize, textColor;

    public MenuItem menuItemChangePicture;

    private SimpleSoundPool sfx;

    public PuzzleController (final Context context,
            final ViewGroup layout_container) {
				
        if (context != null && layout_container != null) {

            this.context = context;

            this.container = layout_container;

            int widthTemp = DeviceDimensionsUtil.getDisplayWidth(context);

            if (!EduardoStuff.isNormalSize(context)) {

                final int h = DeviceDimensionsUtil.getDisplayHeight(context);

                widthTemp = h + (h / 4);
            }

            view_size = widthTemp / PuzzleValues.COLUMN;

            width = view_size * PuzzleValues.COLUMN;

            height = view_size * PuzzleValues.LINE;

            index_image = 0;

            ( ... )
        }
    }

    private void start () { ... }

    private void loadBkgImages () {

        if (context != null) {
            final Resources resources = context.getResources();

            if (resources != null) {

                final int imageLength = EduardoStuff.isNormalSize(context) ? PuzzleValues.IMAGES_RES_ID.length
                        : PuzzleValues.IMAGES_RES_ID_SMALL.length;

                if (imagesList == null) {
                    imagesList = new SparseArrayCompat<BitmapDrawable>(
                            imageLength);

                    BitmapDrawable bkg_image_temp = null;

                    for (int i = 0; i < imageLength; i++) {

                        bkg_image_temp = new BitmapDrawable(
                                resources,
                                UIComponentUtils.decodeSampledBitmapFromResource(
                                        resources,
                                        EduardoStuff.isNormalSize(context) ? PuzzleValues.IMAGES_RES_ID[i]
                                                : PuzzleValues.IMAGES_RES_ID_SMALL[i],
                                        width, height));

                        imagesList.put(i, bkg_image_temp);
                        bkg_image_temp = null;
                    }
                }
            }
        }
    }

    private void createBkgImages () {

        if (piecesImageList == null) {
            piecesImageList = new SparseArrayCompat<Bitmap>(PuzzleValues.SIZE);
        }

        Bitmap croc_bitmap = null;

        Bitmap bitmapTemp = imagesList.get(index_image).getBitmap();

        for (int i = 0; i < PuzzleValues.SIZE; i++) {

            final int line = i / PuzzleValues.COLUMN;

            final int column = ((i < PuzzleValues.COLUMN) ? i
                    : (i % (line * PuzzleValues.COLUMN)));

            final int x = view_size * column;

            final int y = view_size * line;

            if (imagesList != null) {

                croc_bitmap = Bitmap.createBitmap(bitmapTemp, x, y, view_size,
                        view_size);

                if (croc_bitmap != null) {

                    piecesImageList.put(i, croc_bitmap);

                    croc_bitmap = null;
                }
            }
        }
    }

    private void addPuzzle () { ... }

    private void setListeners () { ... }

    private void addDraggableGridView () { ... }

    private void win () { ... }
	
    private Bitmap drawTextToBitmap (final CharSequence textNumber) { ... }

    private void clean () { ... }

    public void deallocateAll () { ... }
	
    private final void showMessageWinner () { ... }

    private final void loadSfx () { ... }

    public final void changeLevel () { ... }
}
