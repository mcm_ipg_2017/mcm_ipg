public class PersistenceManager {

    public static void saveSharedPreferencesString(final Context context,
                                                   final String key, 
												   final String value) {

        if (context != null && key != null && value != null) {
            final SharedPreferences sharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(context
                            .getApplicationContext());

            if (sharedPrefs != null) {
                final SharedPreferences.Editor editor = sharedPrefs.edit();

                if (editor != null) {
                    editor.putString(key, value);
                    editor.commit();
                }
            }
        }
    }

    public static String loadSharedPreferencesString(final Context context,
                                                     final String key, 
													 final String defaultValue) {

        String value = defaultValue;

        if (context != null && key != null && defaultValue != null) {
            final SharedPreferences sharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(context
                            .getApplicationContext());

            if (sharedPrefs != null) {
                value = sharedPrefs.getString(key, defaultValue);
            }
        }
        return value;
    }

    public static void saveSharedPreferencesInt(final Context context,
                                                final String key, 
												final int value) { ... }
    }

    public static int loadSharedPreferencesInt(final Context context,
                                               final String key, 
											   final int defaulValue) { ... }

    public static void saveSharedPreferencesBoolean(final Context context,
                                                    final String key, 
													final boolean value) { ... }

    public static boolean loadSharedPreferencesBoolean(final Context context,
                                                       final String key, 
													   final boolean defaulValue) { ... }

    public static void saveSharedPreferencesLong(final Context context,
                                                 final String key, 
												 final long value) { ... }

    public static long loadSharedPreferencesLong(final Context context,
                                                 final String key, 
												 final long defaulValue) { ... }
}