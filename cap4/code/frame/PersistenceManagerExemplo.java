// Exemplo da utilizacao da funcionalidade da classe PersistenceManager
public final void savePage() {
	if (EduardoStuff.ENABLE_SAVE_PAGE_BOOK) {
		PersistenceManager.saveSharedPreferencesInt(context, PERSISTENCE_KEY_PAGE, pageNumber);
	}
}

public final void loadPage() {
	if (EduardoStuff.ENABLE_SAVE_PAGE_BOOK) {
		pageNumber = PersistenceManager.loadSharedPreferencesInt(context, PERSISTENCE_KEY_PAGE, 0);
	}
	goBookPage(pageNumber);
}