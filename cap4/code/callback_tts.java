// CallBack dentro da classe Frament para inicializar o TTS
public TextToSpeech textToSpeech;

@Override
public final View onCreateView (final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {

	final View view = inflater.inflate(R.layout.layout_viewpager, container, false);

	if (bookController == null) {
		bookController = new BookSPCController(getActivity());

		// Fire off an intent to check if a TTS engine is installed
		final Intent checkIntent = new Intent();

		if (checkIntent != null) {
			checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
			startActivityForResult(checkIntent, MY_DATA_CHECK_CODE);
		}

	}

	createActionBar();

	if (view != null) {
		bookController.startBookText(view);
	}
	return view;
}
	
	
@Override
public void onActivityResult (int requestCode, int resultCode, Intent data) {
	if (requestCode == MY_DATA_CHECK_CODE && EduardoStuff.ENABLE_TTS) {
		if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
			bookController.textToSpeech = new TextToSpeech(getActivity().getApplicationContext(), BookSPCFrag.this);
		} else {
			final Intent installIntent = new Intent();

			if (installIntent != null) {
				installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
				startActivity(installIntent);
			}
			return;
		}
	}
}

@Override
public void onInit (int status) {
	if (EduardoStuff.ENABLE_TTS && status == TextToSpeech.SUCCESS && bookController != null) {

		final Locale locateBR = new Locale("pt_BR");

		final int result = bookController.textToSpeech.setLanguage(locateBR);

		final Locale locatePT = new Locale("pt");

		final int result_2 = bookController.textToSpeech.setLanguage(locatePT);

		if ((result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) || (result_2 == TextToSpeech.LANG_MISSING_DATA || result_2 == TextToSpeech.LANG_NOT_SUPPORTED)) {
			EduardoStuff.ENABLE_TTS = false;
		} else {
			bookController.setTTS();
		}
	} else {
		EduardoStuff.ENABLE_TTS = false;
	}
}