	public final void setTTS () {
        if (imageViewSPC != null && imageViewSPC.size() > 0) {

            final int length = imageViewSPC.size();

            for (int i = 0; i < length; i++) {
                final ImageViewAnimationTTS imageViewAnimationTTS = imageViewSPC.get(i);

                if (imageViewAnimationTTS != null) {
                    imageViewAnimationTTS.setTextToSpeech(textToSpeech);
                }
            }
        }
    }

    private final void stopTTS () {
        if (EduardoStuff.ENABLE_TTS && textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
    }

    private final void createItemsTTS () {
        if (adapter != null) {
            if (imageViewSPC == null) {
                imageViewSPC = new ArrayList<ImageViewAnimationTTS>();

                for (int i = 0; i < BookSPCResources.SPC_BUTTONS_PAGE_1_LENGTH; i++) {

                    final ImageViewAnimationTTS temp = (ImageViewAnimationTTS) adapter.getViewByIndex(0).findViewById(BookSPCResources.SPC_BUTTONS_PAGE_1[i]);

                    if (temp != null) {
                        imageViewSPC.add(temp);
                    }
                }

                for (int i = 0; i < BookSPCResources.SPC_BUTTONS_PAGE_2_LENGTH; i++) {

                    final ImageViewAnimationTTS temp = (ImageViewAnimationTTS) adapter.getViewByIndex(1).findViewById(BookSPCResources.SPC_BUTTONS_PAGE_2[i]);

                    if (temp != null) {
                        imageViewSPC.add(temp);
                    }
                }

                for (int i = 0; i < BookSPCResources.SPC_BUTTONS_PAGE_3_LENGTH; i++) {

                    final ImageViewAnimationTTS temp = (ImageViewAnimationTTS) adapter.getViewByIndex(2).findViewById(BookSPCResources.SPC_BUTTONS_PAGE_3[i]);

                    if (temp != null) {
                        imageViewSPC.add(temp);
                    }
                }

                for (int i = 0; i < BookSPCResources.SPC_BUTTONS_PAGE_4_LENGTH; i++) {

                    final ImageViewAnimationTTS temp = (ImageViewAnimationTTS) adapter.getViewByIndex(3).findViewById(BookSPCResources.SPC_BUTTONS_PAGE_4[i]);

                    if (temp != null) {
                        imageViewSPC.add(temp);
                    }
                }

                for (int i = 0; i < BookSPCResources.SPC_BUTTONS_PAGE_5_LENGTH; i++) {

                    final ImageViewAnimationTTS temp = (ImageViewAnimationTTS) adapter.getViewByIndex(4).findViewById(BookSPCResources.SPC_BUTTONS_PAGE_5[i]);

                    if (temp != null) {
                        imageViewSPC.add(temp);
                    }
                }

                for (int i = 0; i < BookSPCResources.SPC_BUTTONS_PAGE_6_LENGTH; i++) {

                    final ImageViewAnimationTTS temp = (ImageViewAnimationTTS) adapter.getViewByIndex(5).findViewById(BookSPCResources.SPC_BUTTONS_PAGE_6[i]);

                    if (temp != null) {
                        imageViewSPC.add(temp);
                    }
                }

                for (int i = 0; i < BookSPCResources.SPC_BUTTONS_PAGE_7_LENGTH; i++) {

                    final ImageViewAnimationTTS temp = (ImageViewAnimationTTS) adapter.getViewByIndex(6).findViewById(BookSPCResources.SPC_BUTTONS_PAGE_7[i]);

                    if (temp != null) {
                        imageViewSPC.add(temp);
                    }
                }

                final ImageViewAnimationTTS temp = (ImageViewAnimationTTS) adapter.getViewByIndex(7).findViewById(R.id.imageViewSPC_8_1_1);

                if (temp != null) {
                    imageViewSPC.add(temp);
                }
            }
        }
    }
}
