package com.toymobi.framework.persistence;

import ...

public class PersistenceManager {

    private static final String SHARED_PREFERENCES = "PersistenceManager";

    public static void saveSharedPreferencesString(final Context context,
                                                   final String key, final String value) {

        if (context != null && key != null && value != null) {
            final SharedPreferences sharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(context
                            .getApplicationContext());

            if (sharedPrefs != null) {
                final SharedPreferences.Editor editor = sharedPrefs.edit();

                if (editor != null) {
                    editor.putString(key, value);
                    editor.commit();
                }
            }
        }
    }

    public static String loadSharedPreferencesString(final Context context,
                                                     final String key, final String defaultValue) {

        String value = defaultValue;

        if (context != null && key != null && defaultValue != null) {

            final SharedPreferences sharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(context
                            .getApplicationContext());

            if (sharedPrefs != null) {
                value = sharedPrefs.getString(key, defaultValue);
            }
        }

        return value;
    }

    public static void saveSharedPreferencesInt(final Context context,
                                                final String key, final int value) {

        if (context != null && key != null) {

            final SharedPreferences sharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(context
                            .getApplicationContext());

            if (sharedPrefs != null) {
                final SharedPreferences.Editor editor = sharedPrefs.edit();

                if (editor != null) {
                    editor.putInt(key, value);
                    editor.commit();
                }
            }
        }
    }

    public static int loadSharedPreferencesInt(final Context context,
                                               final String key, final int defaulValue) {

        int value = defaulValue;

        if (context != null && key != null) {

            final SharedPreferences sharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(context
                            .getApplicationContext());

            if (sharedPrefs != null) {
                value = sharedPrefs.getInt(key, defaulValue);
            }
        }

        return value;
    }

    public static void saveSharedPreferencesBoolean(final Context context,
                                                    final String key, final boolean value) {

        if (context != null && key != null) {

            final SharedPreferences sharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(context
                            .getApplicationContext());

            if (sharedPrefs != null) {
                final SharedPreferences.Editor editor = sharedPrefs.edit();

                if (editor != null) {
                    editor.putBoolean(key, value);
                    editor.commit();
                }
            }
        }
    }

    public static boolean loadSharedPreferencesBoolean(final Context context,
                                                       final String key, final boolean defaulValue) {

        boolean value = defaulValue;

        if (context != null && key != null) {

            final SharedPreferences sharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(context
                            .getApplicationContext());

            if (sharedPrefs != null) {
                value = sharedPrefs.getBoolean(key, defaulValue);
            }
        }
        return value;
    }

    public static void saveSharedPreferencesLong(final Context context,
                                                 final String key, final long value) {

        if (context != null && key != null) {

            final SharedPreferences sharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(context
                            .getApplicationContext());

            if (sharedPrefs != null) {
                final SharedPreferences.Editor editor = sharedPrefs.edit();

                if (editor != null) {
                    editor.putLong(key, value);
                    editor.commit();
                }
            }
        }
    }

    public static long loadSharedPreferencesLong(final Context context,
                                                 final String key, final long defaulValue) {

        long value = defaulValue;

        if (context != null && key != null) {

            final SharedPreferences sharedPrefs = PreferenceManager
                    .getDefaultSharedPreferences(context
                            .getApplicationContext());

            if (sharedPrefs != null) {
                value = sharedPrefs.getLong(key, defaulValue);
            }
        }
        return value;
    }
}
