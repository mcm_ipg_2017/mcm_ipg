public class BookController {

    private ViewPager viewPager;

    private ReducedCustomPagerAdapter adapter;

    private int pageNumber;

    public CharSequence pageNumberText;

    private Context context;

    private SparseArrayCompat<CharSequence> pageText = null;

    private SparseArrayCompat<Drawable> pageImages = null;

    private SparseArrayCompat<MediaPlayerManager> pageNarration = null;

    public MenuItem menuItemPageNumber;

    public static final String PERSISTENCE_KEY_PAGE = "PERSISTENCE_KEY_PAGE";

    private LayoutInflater layoutInflater;

    public BookController(final Context context) { ... }

    private final void createPageImage() {
        if (context != null) {

            final int size = BookResources.BOOK_PAGES_IMAGE.length;

            if (pageImages == null) {

                pageImages = new SparseArrayCompat<Drawable>(size);

                final Resources resources = context.getResources();

                if (resources != null) {
                    for (int i = 0; i < size; i++) {

                        final int pageIndexID = BookResources.BOOK_PAGES_IMAGE[i];

                        if (pageIndexID != -1) {

                            final Drawable imageDrawable = resources
                                    .getDrawable(pageIndexID);

                            if (imageDrawable != null) {
                                pageImages.put(i, imageDrawable);
                            } else {
                                pageImages.put(i, null);
                            }

                        } else {
                            pageImages.put(i, null);
                        }
                    }
                }
            }
        }
    }

    private final void createPageImageLow() { ... }

    private final void createPageText() {
        if (pageText == null) {

            final int pageLength = BookResources.BOOK_PAGES_TEXT.length;

            pageText = new SparseArrayCompat<CharSequence>(pageLength);

            final int size = BookResources.BOOK_PAGES_TEXT.length;

            for (int i = 0; i < size; i++) {

                final int pageIndexID = BookResources.BOOK_PAGES_TEXT[i];

                final CharSequence page = GetTextRawFile.getTextRawFile(context.getResources(), pageIndexID);

                if (page != null) {
                    pageText.put(i, page);
                }
            }
        }
    }

    private final void loadPageText() {
        if (adapter != null && pageText != null && pageText.size() > 0) {

            for (int index = 0; index < BookResources.BOOK_PAGES_LAYOUT.length; index++) {

                final View viewTemp = adapter.getViewByIndex(index);

                if (viewTemp != null) {

                    final JustifyTextViewCustomFont textViewPageText = (JustifyTextViewCustomFont) viewTemp.findViewById(R.id.book_page_layout_text);

                    if (textViewPageText != null && index < pageText.size()) {

                        final CharSequence text = pageText.get(index);

                        if (text != null && text.length() > 0) {
                            textViewPageText.setTextJustify(text);
                        }
                    }
                }
            }
        }
    }

    private final void loadPageImage() {
        if (adapter != null && pageImages != null && pageImages.size() > 0) {

            for (int index = 0; index < BookResources.BOOK_PAGES_LAYOUT.length; index++) {

                final View viewTemp = adapter.getViewByIndex(index);

                if (viewTemp != null) {

                    final ImageView image = (ImageView) viewTemp
                            .findViewById(R.id.book_page_layout_image);

                    if (image != null) {

                        final int pageImageSize = pageImages.size();

                        if (pageImageSize > index) {
                            final Drawable imageDrawable = pageImages
                                    .get(index);

                            if (imageDrawable != null) {
                                image.setImageDrawable(imageDrawable);
                            }
                        }
                    }
                }
            }
        }
    }
	
	
	private final void createNarration() {
        if (pageNarration == null) {

            final int size = BookResources.AUDIOS_LENGTH;

            pageNarration = new SparseArrayCompat<MediaPlayerManager>(size);

            for (int i = 0; i < size; i++) {
                if (BookResources.audiosPath[i] == BookResources.PAGE_WITHOUT_NARRATION) {
                    pageNarration.put(i, null);
                } else {
                    final MediaPlayerManager mediaPlayerManager = getSoundMedia(BookResources.audiosPath[i]);
                    if (mediaPlayerManager != null) {
                        pageNarration.put(i, mediaPlayerManager);
                    }
                }
            }
        }
    }
	
	private final void createAdapter(final LayoutInflater layoutInflater) { ... }

    private final void createViewPager() { ... }
	
	private final void start() { ... }
   
	public final MediaPlayerManager getSoundMedia(final int resIdSoundMedia) { ... }
	
	public final void startBookText(final View view) { ... }

    private final void goBookPage(final int page) { ... }

    public final void bookPagePreviews() { ... }

    public final void bookPageNext() { ... }

    public final void pauseNarration() { ... }

    public final void playNarration() { ... }

    public final void resumeNarration() { ... }

    public final void stopNarration() { ... }

    public final void changePageNumberText() { ... }

    public final void clearAll() { ... }

    public final void savePage() { ... }

    public final void loadPage() { ... }
}
