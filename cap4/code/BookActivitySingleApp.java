package com.toymobi.single_book_app;

import ...

public class BookActivitySingleApp extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.toymobi.book.R.layout.main_layout_book);
        startBookFrag();
    }

    private final void startBookFrag() {

        final FragmentManager fragmentManager = getSupportFragmentManager();

        if (fragmentManager != null) {
            final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            if (fragmentTransaction != null) {
                BookFrag fragmentBook = (BookFrag) fragmentManager.findFragmentByTag(BookFrag.FRAGMENT_TAG_BOOK_TEXT);

                if (fragmentBook == null) {
                    fragmentBook = new BookFrag();

                    fragmentTransaction.add(com.toymobi.book.R.id.main_layout_fragment_id, fragmentBook, BookFrag.FRAGMENT_TAG_BOOK_TEXT);

                    fragmentTransaction.commit();
                }
            }
        }
    }
}
