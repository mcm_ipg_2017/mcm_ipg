package com.toymobi.book;

import ...

public class BookFrag extends Fragment {

    public static final boolean isBuild = true;

    private SimpleSoundPool sfx;

    private MusicBackgroundController musicBackgroundController;

    private BookController bookController;

    public static final String FRAGMENT_TAG_BOOK_TEXT = "FRAGMENT_TAG_BOOK_TEXT";

    private VibrationFeedback vibrationFeedback;

    @Override
    public final void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        createSFX();

        if (musicBackgroundController == null) {
            musicBackgroundController = new MusicBackgroundController(getActivity(), R.string.path_sound_book);
        }

        if (vibrationFeedback == null) {
            vibrationFeedback = new VibrationFeedback(getActivity());
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (bookController != null) {
            bookController.loadPage();
        }

        if (musicBackgroundController != null) {
            musicBackgroundController.playMusic();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (bookController != null) {
            bookController.pauseNarration();
            bookController.savePage();
        }

        if (musicBackgroundController != null) {
            musicBackgroundController.stopMusic();
        }
    }

    @Override
    public final void onStop() {
        super.onStop();

        if (bookController != null) {
            bookController.stopNarration();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        deallocate();
    }

    @Override
    public final void onCreateOptionsMenu(final Menu menu, final MenuInflater inflater) {

        if (menu != null) {
            menu.clear();

            if (inflater != null) {
                inflater.inflate(R.menu.menu_book_text, menu);

                // Set an icon in the ActionBar
                final MenuItem itemSound = menu.findItem(R.id.sound_menu_book);

                if (itemSound != null) {
                    if (GlobalSettings.soundEnable) {
                        itemSound.setIcon(R.drawable.icon_sound_on);
                    } else {
                        itemSound.setIcon(R.drawable.icon_sound_off);
                    }
                }

                final MenuItem itemPageNumber = menu.findItem(R.id.number_page_menu_book);

                if (itemPageNumber != null) {

                    if (bookController == null) {
                        bookController = new BookController(getActivity());
                    }
                    if (bookController != null) {
                        bookController.menuItemPageNumber = itemPageNumber;
                        bookController.changePageNumberText();
                    }
                }
            }
        }
    }

    @Override
    public final View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.layout_viewpager, container, false);

        if (bookController == null) {
            bookController = new BookController(getActivity());
        }

        createActionBar();

        if (view != null) {
            bookController.startBookText(view);
        }
        return view;
    }

    @Override
    public final boolean onOptionsItemSelected(final MenuItem item) {
        if (item != null) {

            final int item_id = item.getItemId();

            if (item_id > 0) {

                playFeedbackButton();

                if (item_id == R.id.back_menu_book) {
                    if (isBuild) {
                        EduardoStuff.showMessageBackMainMenuSPC(getActivity());
                    } else {
                        EduardoStuff.showMessageExitSPC(getActivity());
                    }
                } else if (item_id == R.id.sound_menu_book) {
                    GlobalSettings.soundEnable ^= true;

                    if (GlobalSettings.soundEnable) {
                        item.setIcon(R.drawable.icon_sound_on);
                        bookController.playNarration();
                        if (musicBackgroundController != null) {
                            musicBackgroundController.resumeMusic();
                        }
                    } else {
                        item.setIcon(R.drawable.icon_sound_off);
                        bookController.stopNarration();
                        if (musicBackgroundController != null) {
                            musicBackgroundController.pauseMusic();
                        }
                    }
                } else if (item_id == R.id.preview_page_menu_book) {
                    bookController.bookPagePreviews();
                } else if (item_id == R.id.next_page_menu_book) {
                    bookController.bookPageNext();
                } else {
                    super.onOptionsItemSelected(item);
                }
            }
        }
        return true;
    }

    private final void createActionBar() {

        setHasOptionsMenu(true);

        final ActionBar actionBar = ((AppCompatActivity) getActivity())
                .getSupportActionBar();

        if (actionBar != null) {

            actionBar.setDisplayHomeAsUpEnabled(false);

            final SpannableString s = new SpannableString(getText(R.string.book_name));

            s.setSpan(new TypefaceSpan("Roboto-Black.ttf"), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            actionBar.setTitle(s);

            final int titleId = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");

            if (titleId > 0) {

                final TextView title = (TextView) ((AppCompatActivity) getActivity()).findViewById(titleId);

                if (title != null) {
                    title.setTextAppearance(((AppCompatActivity) getActivity()), R.style.TextTitleActionBar);
                }
            }
        }
    }

    private void createSFX() {
        if (sfx == null) {
            sfx = new SimpleSoundPool(getActivity().getApplicationContext(), R.raw.sfx_normal_click);
        }
    }

    private void deallocate() {
        if (musicBackgroundController != null) {
            musicBackgroundController.stopMusic();
        }

        if (bookController != null) {
            bookController.stopNarration();
            bookController.clearAll();
        }

        if (sfx != null) {
            sfx.release();
            sfx = null;
        }

        if (musicBackgroundController != null) {
            musicBackgroundController.stopMusic();
            musicBackgroundController = null;
        }
    }

    public final void playFeedbackButton() {
        if (vibrationFeedback != null) {
            vibrationFeedback.vibrate();
        }

        if (sfx != null && GlobalSettings.soundEnable) {
            sfx.playSound(R.raw.sfx_normal_click);
        }
    }
}