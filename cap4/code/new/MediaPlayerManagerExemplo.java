// Exemplo do uso da classe MediaPlayerManager
// musicPath: caminho do ficheiro do audio localizada geralmente na pasta 
// do projeto Android ou URI numa pasta externa
if (musicPath != null && musicPath.length() > 0) {
	music = new MediaPlayerManager(musicPath, context);
	if (music != null) {
		if (GlobalSettings.soundEnable) {
			music.playMedia(false, GlobalSettings.soundEnable);
		} else {
			music.pauseMedia();
		}
	}
}