public class GameDragAndDropController {

    private DragAndDropGameView dragAndDropGameView;

    private ImageViewAnimationTTS itemImage;

    public OnNextItemListener onNextItemListener;

    private VibrationFeedback vibrationFeedback;

    private SimpleSoundPool sfx;

    private Random random;

    private Resources resources;

    private SparseArrayCompat<DragItem> dragItems;

    private static int itemIndex = 0;

    private Handler nextItemHandler;

    public TextToSpeech textToSpeech;

    public GameDragAndDropController (final Context context,
            final View rootViewLayout, final LayoutInflater layoutInflater) {
        if (context != null) {

            resources = context.getResources();

            ( ... )

            if (random == null) {
                random = new Random();
            }

            if (rootViewLayout != null) {
                dragAndDropGameView = (DragAndDropGameView) rootViewLayout
                        .findViewById(R.id.word_view_layout_container);

                itemImage = (ImageViewAnimationTTS) rootViewLayout
                        .findViewById(R.id.item_image);

            }

            if (dragItems == null) {
                dragItems = new SparseArrayCompat<DragItem>(
                        ItemStruct.ITEM_SIZE);
            }

            startItemLetter();
        }
    }

    public final void playFeedbackButton () { ... }

    public final void deallocate () { ... }

    private final void startItemLetter () { ... }

    private final void startLevel () { ... }

    public final void nextItemLetter () { ... }

    private final void createItemLetters () { ... }

    private final void stopTTS () { ... }

    public final void setTTS () { ... }
}
