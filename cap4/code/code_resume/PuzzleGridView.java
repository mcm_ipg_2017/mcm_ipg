public class PuzzleGridView extends ViewGroup implements View.OnTouchListener {

    private static final int ANIMATION_TIME = 150;
	
	private static final int START_POINT_VALUE = -1;

    private int column, view_size;

    private static final int padding = 1;

    private float lastDeltaTime = 0;

    private Handler handler = new Handler();

    private int dragged = START_POINT_VALUE, lastX = START_POINT_VALUE, 
			    lastY = START_POINT_VALUE, lastTarget START_POINT_VALUE;

    private boolean isTouching = false;

    private List<Integer> newPositionList;

    private OnPuzzleReorderListener onRearrangeListener;

    private SimpleSoundPool sfx;

    public PuzzleGridView (final Context context, final int view_size, final int column) { ... }

    private Runnable updateTask = new Runnable() { ... }

    @Override
    public void addView (final View child) { ... }
	
    @Override
    public void removeViewAt (final int index) { ... }

    @Override
    protected void onLayout (final boolean changed, final int l, final int t, final int r, final int b) { ... }

    @Override
    protected int getChildDrawingOrder (final int childCount, final int i) { ... }

    private int getIndexFromCoordinate (final int x, final int y) { ... }

    private int getTargetFromCoordinate (int target_coordinate) { ... }

    private int getTargetFromCoordinate (final int x, final int y) { ... }

    private void animateDragged () { ... }

    private void animateGap (final int target) { ... }

    private void reorderChildren () { ... }

    private int getLastIndex () { ... }
    
	public void setOnRearrangeListener (final OnPuzzleReorderListener l){ ... }

    public final void deallocate () { ... }
}