package toymobi.eduardo.dragdropgame;

import ...

public class GameDragAndDropController {

    private DragAndDropGameView dragAndDropGameView;

    private ImageViewAnimationTTS itemImage;

    public OnNextItemListener onNextItemListener;

    private VibrationFeedback vibrationFeedback;

    private SimpleSoundPool sfx;

    private Random random;

    private Resources resources;

    private SparseArrayCompat<DragItem> dragItems;

    private static int itemIndex = 0;

    private Handler nextItemHandler;

    public TextToSpeech textToSpeech;

    public GameDragAndDropController (final Context context,
            final View rootViewLayout, final LayoutInflater layoutInflater) {
        if (context != null) {

            resources = context.getResources();

            if (vibrationFeedback == null) {
                vibrationFeedback = new VibrationFeedback(context);
            }

            if (sfx == null) {
                sfx = new SimpleSoundPool(context, R.raw.sfx_normal_click,
                        R.raw.sfx_winner);
            }

            if (random == null) {
                random = new Random();
            }

            if (rootViewLayout != null) {
                dragAndDropGameView = (DragAndDropGameView) rootViewLayout
                        .findViewById(R.id.word_view_layout_container);

                itemImage = (ImageViewAnimationTTS) rootViewLayout
                        .findViewById(R.id.item_image);

            }

            if (dragItems == null) {
                dragItems = new SparseArrayCompat<DragItem>(
                        ItemStruct.ITEM_SIZE);
            }

            startItemLetter();
        }
    }

    public final void playFeedbackButton () {
        if (vibrationFeedback != null) {
            vibrationFeedback.vibrate();
        }

        if (sfx != null && GlobalSettings.soundEnable) {
            sfx.playSound(R.raw.sfx_normal_click);
        }
    }

    public final void deallocate () {

        if (nextItemHandler != null) {
            nextItemHandler = null;
        }
        stopTTS();

        if (sfx != null) {
            sfx.release();
            sfx = null;
        }

        if (dragAndDropGameView != null) {
            dragAndDropGameView.deallocate();
            dragAndDropGameView = null;
        }

        for (int i = 0; i < ItemStruct.ITEM_SIZE; i++) {
            DragItem dragItem = dragItems.get(i);

            if (dragItem != null) {
                dragItem.deallocate();
                dragItem = null;
            }
        }
        dragItems.clear();
        dragItems = null;
    }

    private void startItemLetter () {

        if (dragAndDropGameView != null) {

            createItemLetters();

            startLevel();
        }
    }

    private void startLevel () {

        if (dragAndDropGameView != null) {
            itemIndex = 0;

            dragAndDropGameView.eraserSpot();

            dragAndDropGameView.setDragItem(dragItems.get(itemIndex));

            itemImage.setImageDrawable(dragItems.get(itemIndex).image);

            itemImage.setText(dragItems.get(itemIndex).name.toString());

            dragAndDropGameView.start();
        }
    }

    public final void nextItemLetter () {

        if (dragAndDropGameView != null) {

            if (itemIndex < ItemStruct.ITEM_SIZE - 1) {
                itemIndex++;
            } else {
                itemIndex = 0;
            }

            dragAndDropGameView.eraserSpot();

            dragAndDropGameView.setDragItem(dragItems.get(itemIndex));

            itemImage.destroyDrawingCache();
            itemImage.setImageDrawable(null);
            itemImage.setImageDrawable(dragItems.get(itemIndex).image);

            itemImage.setText(dragItems.get(itemIndex).name.toString());

            dragAndDropGameView.start();
        }
    }

    private void createItemLetters () {
        if (dragItems != null && dragItems.size() == 0) {

            for (int i = 0; i < ItemStruct.ITEM_SIZE; i++) {
                final DragItem dragItem = new DragItem(resources,
                        ItemStruct.item_text_id[i], ItemStruct.item_image_id[i]);

                if (dragItem != null) {
                    dragItems.put(i, dragItem);
                }
            }
        }

        if (onNextItemListener == null) {
            onNextItemListener = new OnNextItemListener() {

                @Override
                public void nextItem () {
                    if (sfx != null) {
                        sfx.playSound(R.raw.sfx_winner);
                    }

                    if (nextItemHandler == null) {
                        nextItemHandler = new Handler();
                    }

                    nextItemHandler.post(new Runnable() {
                        @Override
                        public void run () {
                            nextItemLetter();
                        }
                    });
                }
            };
            dragAndDropGameView.onNextItemListener = onNextItemListener;
        }
    }

    private void stopTTS () {
        if (EduardoStuff.ENABLE_TTS && textToSpeech != null) {
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
    }

    public final void setTTS () {
        if (itemImage != null) {
            itemImage.setTextToSpeech(textToSpeech);
        }
    }
}
