public class UIComponentUtils {

    private static final Bitmap BITMAP_EMPTY = null;

    public static StateListDrawable createStateListDrawable(final Theme theme,
                                                            final int attrOn,
                                                            final int attrOff,
                                                            final int gravity) { ... }

    public static Drawable getDrawableByImageId(final Theme theme,
                                                final int res_image_id) { ... }

    public static Bitmap getBitmapById(final Context context,
                                       final int image_res_id) { ... }

    public static int getDimensionPixelSizeByTheme(final Theme theme,
                                                   final int attrValue, 
                                                   final int defaultValue) { ... }

    public static Bitmap resizeBitmapByDrawableId(final Context context,
                                                  final int new_width, 
                                                  final int new_height, 
                                                  final int resIdTheme) { ... }

    public static Bitmap resizeBitmapById(final Context context,
                                          final int new_width, 
                                          final int new_height, 
                                          int image_res_id) { ... }

    public static Bitmap resizeBitmapById(final Context context,
                                          final int new_height, 
										  int image_res_id) { ... }

    public static Bitmap decodeSampledBitmapFromResource(Resources res,
                                                         int resId, int reqWidth, 
														 int reqHeight) { ... }

    private static int calculateInSampleSize(BitmapFactory.Options options,
                                             int reqWidth, int reqHeight) { ... }
}