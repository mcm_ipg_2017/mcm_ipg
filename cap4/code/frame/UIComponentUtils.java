package com.toymobi.framework.image;

import ...

public class UIComponentUtils {
	
    private static final Bitmap BITMAP_EMPTY = null;

    public static StateListDrawable createStateListDrawable (final Theme theme,
            final int attrOn, final int attrOff, final int gravity) {

        StateListDrawable stateListDrawable = null;
		
        if (theme != null && attrOn > 0 && attrOff > 0) {

            stateListDrawable = new StateListDrawable();

            final TypedArray typedArray = theme
                    .obtainStyledAttributes(new int[] { attrOn, attrOff });

            final BitmapDrawable iconToggleOn = (BitmapDrawable) typedArray
                    .getDrawable(0);

            if (iconToggleOn != null) {
                iconToggleOn.setFilterBitmap(true);
                iconToggleOn.setGravity(Gravity.CENTER);
            }

            final BitmapDrawable iconToggleOff = (BitmapDrawable) typedArray
                    .getDrawable(1);

            if (iconToggleOff != null) {
                iconToggleOff.setFilterBitmap(true);
                iconToggleOff.setGravity(gravity);
            }

            stateListDrawable.addState(
                    new int[] { android.R.attr.state_checked }, iconToggleOn);

            stateListDrawable.addState(new int[] {}, iconToggleOff);

            typedArray.recycle();

        }

        return stateListDrawable;
    }

    public static Drawable getDrawableByImageId (final Theme theme,
            final int res_image_id) {

        Drawable drawable = null;

        if (theme != null && res_image_id > 0) {

            final TypedArray typedArray = theme
                    .obtainStyledAttributes(new int[] { res_image_id });

            if (typedArray != null) {
                drawable = typedArray.getDrawable(0);
                typedArray.recycle();
            }
        }

        return drawable;
    }

    public static Bitmap getBitmapById (final Context context,
            final int image_res_id) {

        if (context != null && image_res_id > 0) {
            Drawable drawable = null;

            if (context != null && image_res_id != -1) {
                drawable = context.getResources().getDrawable(image_res_id);

            }
            return ((drawable != null) ? ((BitmapDrawable) drawable).getBitmap() : null);
        } else {
            return null;
        }
    }

    public static int getDimensionPixelSizeByTheme (final Theme theme,
            final int attrValue, final int defaultValue) {

        int value = 0;

        if (theme != null && attrValue > 0) {

            final TypedArray typedArray = theme
                    .obtainStyledAttributes(new int[] { attrValue });

            if (typedArray != null) {
                value = typedArray.getDimensionPixelSize(0, defaultValue);
                typedArray.recycle();
            }
        }
        return value;
    }

    public static Bitmap resizeBitmapByDrawableId (final Context context,
            final int new_width, final int new_height, final int resIdTheme) {

        if (context != null && new_width > 0 && new_height > 0
                && resIdTheme > 0) {

            final Bitmap source = UIComponentUtils.getBitmapById(context, resIdTheme);

            if (source != null) {
                final Matrix matrix = new Matrix();

                if (matrix != null) {
                    final int width = source.getWidth();
                    final int height = source.getHeight();
                    final float scaleWidth = ((float) new_width) / width;
                    final float scaleHeight = ((float) new_height) / height;
                    matrix.postScale(scaleWidth, scaleHeight);

                    final Bitmap result = Bitmap.createBitmap(source, 0, 0, width, height, matrix, true);

                    return result;
                }
            }
        }
        return BITMAP_EMPTY;
    }

    public static Bitmap resizeBitmapById (final Context context,
            final int new_width, final int new_height, int image_res_id) {

        if (context != null && new_width > 0 && new_height > 0
                && image_res_id != -1) {

            final Bitmap source = UIComponentUtils.getBitmapById(context, image_res_id);

            if (source != null) {
                final Matrix matrix = new Matrix();

                if (matrix != null) {
                    final int width = source.getWidth();
                    final int height = source.getHeight();
                    final float scaleWidth = ((float) new_width) / width;
                    final float scaleHeight = ((float) new_height) / height;
                    matrix.postScale(scaleWidth, scaleHeight);

                    final Bitmap result = Bitmap.createBitmap(source, 0, 0,
                            width, height, matrix, true);

                    return result;
                }
            }
        }
        return BITMAP_EMPTY;
    }

    public static Bitmap resizeBitmapById (final Context context,
            final int new_height, int image_res_id) {

        if (context != null && new_height > 0 && image_res_id > 0) {

            final Bitmap source = UIComponentUtils.getBitmapById(context,
                    image_res_id);

            if (source != null) {
                final int oldWidth = source.getWidth();

                if (source != null) {
                    final Matrix matrix = new Matrix();

                    if (matrix != null) {
                        final int width = source.getWidth();
                        final int height = source.getHeight();
                        final float scaleWidth = ((float) oldWidth) / width;
                        final float scaleHeight = ((float) new_height) / height;
                        matrix.postScale(scaleWidth, scaleHeight);

                        final Bitmap result = Bitmap.createBitmap(source, 0, 0,
                                width, height, matrix, true);

                        return result;
                    }
                }
            }
        }
        return BITMAP_EMPTY;
    }

    public static Bitmap decodeSampledBitmapFromResource (Resources res,
            int resId, int reqWidth, int reqHeight) {

        if (res != null && resId != -1 && reqWidth > 0 && reqHeight > 0) {

            final BitmapFactory.Options options = new BitmapFactory.Options();

            options.inJustDecodeBounds = true;

            BitmapFactory.decodeResource(res, resId, options);

            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

            options.inJustDecodeBounds = false;

            Bitmap sourceResized = null;

            Bitmap source = BitmapFactory.decodeResource(res, resId, options);

            if (source != null) {
                sourceResized = Bitmap.createScaledBitmap(source, reqWidth,
                        reqHeight, true);

                if (source != null) {
                    if (source.isRecycled()) {
                        source.isRecycled();
                        source = null;
                    }
                }
            }
            return sourceResized;
        } else {
            return null;
        }
    }

    private static int calculateInSampleSize (BitmapFactory.Options options,
            int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }
}