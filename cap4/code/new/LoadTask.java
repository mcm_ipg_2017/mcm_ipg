public class LoadTask extends AsyncTask<Void, Void, Void> {
    
	private final OnLoadTaskFinish onLoadTaskFinish;
    private final OnLoadTaskExecute onLoadTaskExecute;

    public LoadTask(final OnLoadTaskFinish onLoadTaskFinish,
                    final OnLoadTaskExecute onLoadTaskExecute) {
        this.onLoadTaskFinish = onLoadTaskFinish;
        this.onLoadTaskExecute = onLoadTaskExecute;
    }

    @Override
    protected void onPreExecute() { ... }

    @Override
    protected Void doInBackground(final Void... params) {
        if (onLoadTaskExecute != null) {
            onLoadTaskExecute.onLoadTaskExecute();
        }
    }

    @Override
    protected void onPostExecute(final Void result) {
        if (onLoadTaskFinish != null) {
            onLoadTaskFinish.onLoadTaskFinish();
        }
    }
}
