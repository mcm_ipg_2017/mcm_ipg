public class ImageViewAnimationTTS extends ImageView implements OnClickListener {

    private Animation animation;
    private OnClickListener clickListener;
    private String text;
    private TextToSpeech textToSpeech;

    public ImageViewAnimationTTS(final Context context) { ... }

    public ImageViewAnimationTTS(final Context context,
                                 final AttributeSet attrs) { ... }

    public ImageViewAnimationTTS(final Context context,
                                 final AttributeSet attrs, final int defStyle) { ... }

    @Override
    public final void setOnClickListener(final OnClickListener onClickListener) {
        if (onClickListener == this) {
            super.setOnClickListener(onClickListener);
        } else {
            clickListener = onClickListener;
        }
    }

    @Override
    public final void onClick(final View view) {
        if (view != null) {
            if (animation != null) {
                view.startAnimation(animation);
            }

            if (textToSpeech != null) {
                if (text != null && text.length() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        speakNewApiTTS();
                    } else {
                        speakOldApiTTS();
                    }
                }
            }
            if (clickListener != null) {
                clickListener.onClick(this);
            }
        }
    }

    private final void createAnimation(final AttributeSet attrs) {
        if (!isInEditMode()) {
            if (animation == null) {

                final TypedArray typedArray = getContext()
                        .obtainStyledAttributes(attrs,
                                R.styleable.imageview_animation);

                if (typedArray != null) {
                    final int reference = typedArray.getResourceId(
                            R.styleable.imageview_animation_layout_animation, 0);

                    if (reference != 0) {
                        animation = AnimationUtils.loadAnimation(getContext(),
                                reference);
                    }

                    text = typedArray
                            .getString(R.styleable.imageview_animation_text_animation);

                    typedArray.recycle();
                }
            }
        }
    }

    public void setTextToSpeech(final TextToSpeech textToSpeech) { ... }

    public final void setText(final String textTTS) { ... }

    private final void speakOldApiTTS() { ... }
	
    private final void speakNewApiTTS() { ... }
}
