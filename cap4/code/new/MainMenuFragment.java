public class MainMenuFragment extends Fragment {

    private SimpleSoundPool sfx;

    private VibrationFeedback vibrationFeedback;

    private MusicBackgroundController musicBackgroundController;

    private static final int[] options = { R.id.option_1, R.id.option_2,
            R.id.option_3, R.id.option_4, R.id.option_5, R.id.option_6,
            R.id.option_7 };

    public static final String MAIN_MENU_FRAGMENT_TAG = "MAIN_MENU_FRAGMENT";

    private OnClickListener optionOnClickListener;

    private CharSequence pageInfoText = null;

    @Override
    public final View onCreateView (final LayoutInflater inflater,
            final ViewGroup container, final Bundle savedInstanceState) { ... }

	@Override
    public void onCreate (Bundle savedInstanceState) { ... }
	
    @Override
    public final void onCreateOptionsMenu (final Menu menu, 
										   final MenuInflater inflater) { ... }

    @Override
    public boolean onOptionsItemSelected (final MenuItem item) { ... }

    private void goToInfo () { ... }

    @Override
    public final void onResume () { ... }

    @Override
    public final void onPause () { ... }

    @Override
    public final void onStop () { ... }

    @Override
    public final void onDestroy (){ ... }

    private void createActionBar () { ... }

    public final void playFeedbackButton () { ... }

	private void createStart (final View view) {

       ( ... )

        if (optionOnClickListener == null) {

            optionOnClickListener = new OnClickListener() {

                @Override
                public void onClick (final View view) {
                    if (view != null) {
                        final int viewID = view.getId();

                        switch (viewID) {
                        case R.id.option_1:
                            goOption1();
                            break;
                        case R.id.option_2:
                            goOption2();
                            break;
                        case R.id.option_3:
                            goOption3();
                            break;
                        case R.id.option_4:
                            goOption4();
                            break;
                        case R.id.option_5:
                            goToOption5();
                            break;
                        case R.id.option_6:
                            goToOption6();
                            break;
                        case R.id.option_7:
                            goToOption7();
                            break;
                        }
                    }
                }
            };
        }

		for (int i = 0; i < options.length; i++) {
            final View optionView = view.findViewById(options[i]);

            if (optionView != null && optionOnClickListener != null) {
                optionView.setOnClickListener(optionOnClickListener);
            }
        }
    }
	
    private void goOption1 () { ... }
	
	private void goOption2 () { ... }
	
	private void goOption3 () { ... }
	
	private void goOption4 () { ... }
	
	private void goOption5 () { ... }
	
	private void goOption6 () { ... }
	
    private void deallocate () { ... }

}