public class SimpleSoundPool {
	
    private static final int MAX_QUALITY = 100;
    private static final float NORMAL_SPEED = 1.0f;
    private static final float VOLUME = 1.0f;
    private static final int VOLUME_NONE = -1;

    private SoundPool soundPool;

    private SparseIntArray sfxMapInt;

    private boolean loaded = true;

    private AudioManager audioManager;

    public SimpleSoundPool(@NonNull final Context context, final int... sfxResIDArray) {
		
        final int sizeSfx = sfxResIDArray.length;

        if (sizeSfx > 0) {
			
            if (audioManager == null) {
                audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                createNewSoundPool(sizeSfx);
            } else {
                createOldSoundPool(sizeSfx);
            }

            if (sfxMapInt == null) {
                sfxMapInt = new SparseIntArray(sizeSfx);
            }

            soundPool.setOnLoadCompleteListener(new OnLoadCompleteListener() {
                @Override
                public void onLoadComplete(SoundPool soundPool, int sampleId,
                                           int status) {
                    loaded = true;
                }
            });

            /**
             * SoundPool retorna um id apos realizar o loading do resource,
             * sendo que este id eh armazena num mapping para buscar este valor
             * de acordo com o id do resources
             */
            for (int sfxResId : sfxResIDArray) {
                final int indexSoundPool = soundPool.load(context, sfxResId, MAX_QUALITY);
                sfxMapInt.put(sfxResId, indexSoundPool);
            }
        }
    }


    public void playSound(final int sfx) {
		
        final boolean check = (soundPool != null && audioManager != null &&
                loaded && sfxMapInt != null && sfxMapInt.size() > 0);
				
        if (check) {
            float streamVolume = VOLUME_NONE;

            streamVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);

            final float actualVolume = (float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);

            final float maxVolume = (float) audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

            streamVolume = actualVolume / maxVolume;

            final float volume = (streamVolume == VOLUME_NONE) ? VOLUME : streamVolume;

            soundPool.play(sfxMapInt.get(sfx), volume, volume, 1, 0, NORMAL_SPEED);
        }
    }

    private void createNewSoundPool(final int sizeSfx) {
		
        if (soundPool == null) {
            final AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();

            if (attributes != null) {
                soundPool = new SoundPool.Builder().setAudioAttributes(
                        attributes).build();
            }
        }
    }

    private void createOldSoundPool(final int sizeSfx) {
        if (soundPool == null) {
            soundPool = new SoundPool(sizeSfx, AudioManager.STREAM_MUSIC, MAX_QUALITY);
        }
    }

    public void release() { ... }
	
}