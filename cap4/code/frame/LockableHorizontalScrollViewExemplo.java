<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    xmlns:custom_text_view_font="http://schemas.android.com/apk/res-auto"
    xmlns:imageview_animation="http://schemas.android.com/apk/res-auto"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginBottom="@dimen/button_item_main_menu_padding"
    android:layout_marginLeft="@dimen/button_item_main_menu_padding"
    android:layout_marginTop="@dimen/button_item_main_menu_padding"
    android:baselineAligned="false"
    android:orientation="vertical" >

    <com.toymobi.framework.view.scrollview.LockableHorizontalScrollView
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:background="@null"
        android:clickable="true"
        android:fadingEdge="none"
        android:scrollbars="none" >

        <LinearLayout
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:gravity="center_vertical"
            android:orientation="horizontal" >

            <com.toymobi.framework.view.imageview.ImageViewAnimationTTS
                android:id="@+id/imageViewSPC_1_1_1"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:padding="@dimen/button_item_spc_padding"
                android:src="@drawable/spc_eduardo"
                imageview_animation:layout="@anim/rotate_anti_clock_wise"
                imageview_animation:text="@string/spc_1_1_1" />

			...

        </LinearLayout>
    </com.toymobi.framework.view.scrollview.LockableHorizontalScrollView>

    <com.toymobi.framework.view.textview.TextViewCustomFont
        style="@style/TextBook"
        android:text="@string/text_1_1"
        android:textColor="@color/ACTION_BAR_COLOR"
        custom_text_view_font:font="@string/roboto_black" />

</LinearLayout>