package com.toymobi.framework.dimensions;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.TypedValue;

public class DeviceDimensionsUtil {

    public static int getDisplayWidth (final Context context) {
        int widthPixels = -1;
        if (context != null) {
            final Resources resources = context.getResources();
            if (resources != null) {
                final DisplayMetrics displayMetrics = resources
                        .getDisplayMetrics();
                if (displayMetrics != null) {
                    widthPixels = displayMetrics.widthPixels;
                }
            }

        }
        return widthPixels;
    }

    public static final int getDisplayHeight (final Context context) {
        int heightPixels = -1;
        if (context != null) {
            final Resources resources = context.getResources();
            if (resources != null) {
                final DisplayMetrics displayMetrics = resources
                        .getDisplayMetrics();

                if (displayMetrics != null) {
                    heightPixels = displayMetrics.heightPixels;
                }
            }
        }
        return heightPixels;
    }

    public static final float convertDpToPixel (final float dp,
            final Context context) {
        float value = -1;
        if (context != null) {
            final Resources resources = context.getResources();
            if (resources != null) {
                value = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                        dp, resources.getDisplayMetrics());
            }
        }
        return value;
    }

    public static final float convertPixelsToDp (final float px,
            final Context context) {
        float dp = -1;
        if (context != null) {
            final Resources resources = context.getResources();
            if (resources != null) {
                final DisplayMetrics metrics = resources.getDisplayMetrics();
                if (metrics != null) {
                    dp = px / (metrics.densityDpi / 160f);
                }
            }
        }
        return dp;
    }

    public static int getDensity (final Context context) {
        int density = -1;
        if (context != null) {
            final Resources resources = context.getResources();
            if (resources != null) {
                final DisplayMetrics displayMetrics = resources
                        .getDisplayMetrics();
                if (displayMetrics != null) {
                    density = displayMetrics.densityDpi;
                }
            }
        }
        return density;
    }
}
