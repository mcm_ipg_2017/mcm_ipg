package toymobi.eduardo.memorygame;

import ...

public class CardGameIA {

    public static enum SPEED_STATE {
        SPEED_SLOW, SPEED_NORMAL, SPEED_FAST;
    }

    public static enum NUMBER_CARDS_STATE {
        CARDS_6, CARDS_12, CARDS_18;
    }

    public static final int MAX_LEVELS = 8;

    public int cardsCorrect = 0;

    public static SparseArrayCompat<LevelType> levels;

    public static int currentLevel = 0;

    public CardGameIA () {
        createLevel();
    }

    private final void createLevel () {
        if (levels == null) {
            levels = new SparseArrayCompat<LevelType>(MAX_LEVELS);

            levels.put(0, new LevelType(SPEED_STATE.SPEED_SLOW, NUMBER_CARDS_STATE.CARDS_6));

            levels.put(1, new LevelType(SPEED_STATE.SPEED_NORMAL, NUMBER_CARDS_STATE.CARDS_6));

            levels.put(2, new LevelType(SPEED_STATE.SPEED_FAST, NUMBER_CARDS_STATE.CARDS_6));

            levels.put(3, new LevelType(SPEED_STATE.SPEED_SLOW, NUMBER_CARDS_STATE.CARDS_12));

            levels.put(4, new LevelType(SPEED_STATE.SPEED_NORMAL, NUMBER_CARDS_STATE.CARDS_12));

            levels.put(5, new LevelType(SPEED_STATE.SPEED_FAST, NUMBER_CARDS_STATE.CARDS_12));

            levels.put(6, new LevelType(SPEED_STATE.SPEED_SLOW, NUMBER_CARDS_STATE.CARDS_18));

            levels.put(7, new LevelType(SPEED_STATE.SPEED_NORMAL, NUMBER_CARDS_STATE.CARDS_18));

            levels.put(8, new LevelType(SPEED_STATE.SPEED_FAST, NUMBER_CARDS_STATE.CARDS_18));
        }
    }

    public final SPEED_STATE cardRotateSpeed () {

        SPEED_STATE speedState = SPEED_STATE.SPEED_SLOW;

        if (levels != null) {
            speedState = levels.get(currentLevel).speed;
        }
        return speedState;
    }

    public final void setCardRotateSpeed (SPEED_STATE speedState) {
        if (levels != null) {
            levels.get(currentLevel).speed = speedState;
        }
    }

    public final void setCardColumnNumber (NUMBER_CARDS_STATE cardColumnNumber) {
        if (levels != null) {
            levels.get(currentLevel).cardNumber = cardColumnNumber;
        }
    }

    public final NUMBER_CARDS_STATE cardColumnNumber () {

        NUMBER_CARDS_STATE cardColumnNumber = NUMBER_CARDS_STATE.CARDS_6;

        if (levels != null) {
            cardColumnNumber = levels.get(currentLevel).cardNumber;
        }
        return cardColumnNumber;
    }
}
