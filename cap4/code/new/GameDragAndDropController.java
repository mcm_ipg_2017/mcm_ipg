public class GameDragAndDropController {

    private DragAndDropGameView dragAndDropGameView;

    private ImageViewAnimationTTS itemImage;

    public OnNextItemListener onNextItemListener;

    private VibrationFeedback vibrationFeedback;

    private SimpleSoundPool sfx;

    private Random random;

    private Resources resources;

    private SparseArrayCompat<DragItem> dragItems;

    private static int itemIndex = 0;

    private Handler nextItemHandler;

    public TextToSpeech textToSpeech;

    public GameDragAndDropController(@NonNull Context context,
                                     @NonNull final View rootViewLayout,
                                     @NonNull final LayoutInflater layoutInflater) {
										 
        resources = context.getResources();

        ( ... )

        if (random == null) {
            random = new Random();
        }

        dragAndDropGameView = rootViewLayout.findViewById(R.id.word_view_layout_container);

        itemImage = rootViewLayout.findViewById(R.id.item_image);




        if (dragItems == null) {
            dragItems = new SparseArrayCompat<DragItem>(ItemStruct.ITEM_SIZE);
        }

        startItemLetter();
    }

    public final void playFeedbackButton() { ... }

    public final void deallocate() { ... }

    private void startItemLetter() { ... }

    private void startLevel() { ... }

    public final void nextItemLetter() { ... }

    private void createItemLetters() { ... }

    private void stopTTS() { ... }

    public final void setTTS() { ... }
	
}