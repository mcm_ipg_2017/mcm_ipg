package toymobi.eduardo.drawinggame;

import ...

public class DrawingView extends View {
   
   public static final int DRAW_DOT = 0;
    public static final int DRAW_SQUARE = DRAW_DOT + 1;
    public static final int DRAW_CIRCLE = DRAW_SQUARE + 1;
    public static final int DRAW_TRIANGLE = DRAW_CIRCLE + 1;

    private static final int SHAPE_STROKE_SIZE = 6;

    public int drawType = DRAW_DOT;

    private SimpleSoundPool sfx;

    private Path drawPath, poligonTemp;

    private Matrix matrix;

    private Paint drawPaint, canvasPaint;

    private int paintColor = 0xFFFFFFFF;

    private Canvas drawCanvas;

    private Bitmap canvasBitmap;

    private float brushSize, lastBrushSize;

    private boolean erase = false;

    public DrawingView (final Context context) {
        super(context);
        init(context);
    }

    public DrawingView (final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public DrawingView (final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private final void init (final Context context) {
        if (!isInEditMode()) {
            if (context != null) {
                if (sfx == null) {
                    sfx = new SimpleSoundPool(context, R.raw.sfx_drawing);
                }
            }
            brushSize = getResources().getInteger(R.integer.medium_size);
            lastBrushSize = brushSize;
            drawPath = new Path();
            drawPaint = new Paint();
            drawPaint.setColor(paintColor);
            drawPaint.setAntiAlias(true);
            drawPaint.setStrokeWidth(brushSize);
            drawPaint.setStyle(Paint.Style.STROKE);
            drawPaint.setStrokeJoin(Paint.Join.ROUND);
            drawPaint.setStrokeCap(Paint.Cap.ROUND);
            canvasPaint = new Paint(Paint.DITHER_FLAG);
        }
    }

    @Override
    protected final void onSizeChanged (final int width, final int heigth, final int oldWidth, final int oldHeigth) {

        super.onSizeChanged(width, heigth, oldWidth, oldHeigth);

        canvasBitmap = Bitmap.createBitmap(width, heigth, Bitmap.Config.ARGB_8888);

        drawCanvas = new Canvas(canvasBitmap);
    }

    @Override
    protected final void onDraw (final Canvas canvas) {
        if (canvas != null && canvasBitmap != null && canvasPaint != null && drawPath != null && drawPaint != null) {
            canvas.drawBitmap(canvasBitmap, 0, 0, canvasPaint);
            canvas.drawPath(drawPath, drawPaint);
        }
    }

    @Override
    public final boolean onTouchEvent (final MotionEvent event) {
        final float touchX = event.getX();
        final float touchY = event.getY();

        switch (event.getAction()) {
        case MotionEvent.ACTION_DOWN:
            if (drawType == DRAW_DOT) {
                drawPath.moveTo(touchX, touchY);
                drawPath.lineTo(touchX + 1, touchY + 1);
                invalidate();
            } else {
                drawShape(touchX, touchY);
            }
            return true;
        case MotionEvent.ACTION_MOVE:
            if (drawType == DRAW_DOT) {
                drawPath.lineTo(touchX, touchY);
                invalidate();
            } else {
                drawShape(touchX, touchY);
            }

            return true;
        case MotionEvent.ACTION_UP:
            if (sfx != null && GlobalSettings.soundEnable) {
                sfx.playSound(R.raw.sfx_drawing);
            }

            if (drawType == DRAW_DOT) {
                drawShape(touchX, touchY);
            }
            return true;
        default:
            return super.onTouchEvent(event);
        }
    }

    private void drawShape (final float touchX, final float touchY) {
        switch (drawType) {
        
		case DRAW_DOT:
            drawCanvas.drawPath(drawPath, drawPaint);
            drawPath.reset();
            break;

        case DRAW_SQUARE:
            drawPaint.setStrokeWidth(SHAPE_STROKE_SIZE);
            drawPath.addPath(getPoligon(touchX, touchY, (int) brushSize, 4, 45f));
            drawCanvas.drawPath(drawPath, drawPaint);
            drawPath.reset();
            break;

        case DRAW_CIRCLE:
            drawPaint.setStrokeWidth(SHAPE_STROKE_SIZE);
            drawPath.addCircle(touchX, touchY, brushSize, Path.Direction.CCW);
            drawCanvas.drawPath(drawPath, drawPaint);
            drawPath.reset();
            break;

        case DRAW_TRIANGLE:
            drawPaint.setStrokeWidth(SHAPE_STROKE_SIZE);
            drawPath.addPath(getPoligon(touchX, touchY, (int) brushSize, 3, 30f));
            drawCanvas.drawPath(drawPath, drawPaint);
            drawPath.reset();
            break;
        }
        invalidate();
    }

    public final void setColor (final int newColor) {
        invalidate();
        paintColor = newColor;
        if (drawPaint != null) {
            drawPaint.setColor(paintColor);
            drawPaint.setShader(null);
        }
    }

    public final void setBrushSize (final float newSize) {
        float pixelAmount = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, newSize, getResources().getDisplayMetrics());
        brushSize = pixelAmount;
        if (drawPaint != null) {
            drawPaint.setStrokeWidth(brushSize);
        }
    }

    public final void setLastBrushSize (final float lastSize) {
        lastBrushSize = lastSize;
    }

    public final float getLastBrushSize () {
        return lastBrushSize;
    }

    public final void setErase (final boolean isErase) {
        erase = isErase;
        
		if (drawCanvas != null) {
            if (erase) {
                drawPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
            } else {
                drawPaint.setXfermode(null);
            }
        }
    }

    public final void startNewPaper () {
        if (drawCanvas != null) {
            drawPath.reset();
            drawCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
            invalidate();
        }
    }

    private final Path getPoligon (final float centerX, final float centerY, final int radius, final int numberPoints, final float degrees) {

        if (poligonTemp == null) {
            poligonTemp = new Path();
        }

        poligonTemp.reset();

        double ang = Math.PI * 2 / numberPoints;

        poligonTemp.moveTo((float) (centerX + radius * Math.cos(0)),(float) (centerY + radius * Math.sin(0)));

        for (int i = 1; i < numberPoints; i++) {
            poligonTemp.lineTo((float) (centerX + radius * Math.cos(ang * i)),(float) (centerY + radius * Math.sin(ang * i)));
        }

        if (matrix == null) {
            matrix = new Matrix();
        }

        matrix.reset();

        matrix.postRotate(degrees, centerX, centerY);

        poligonTemp.transform(matrix);

        poligonTemp.close();

        return poligonTemp;
    }
}
