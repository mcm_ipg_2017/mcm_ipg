public Handler cardsStateHandler = new Handler() {
	public void handleMessage (final Message message) {
		if (message != null) {
			final int responde_message = message.what;

			switch (responde_message) {
				
				case CardValues.RESET_CARDS_STATE:
					reset();
					break;
				
				case CardValues.DISABLE_ALL_CARDS_STATE:
					disableAllCardsState();
					break;
				
				case CardValues.ENABLE_ALL_CARDS_STATE:
					enableAllCardsState();
					break;
				
				case CardValues.OPEN_CARDS_STATE:
					openCardsState();
					break;
				
				case CardValues.CLOSE_CARDS_STATE:
					closeCardsState();
					break;
				
				case CardValues.OPEN_ALL_CARDS_STATE:
					openAllCardsState();
					break;
				
				case CardValues.CLOSE_ALL_CARDS_STATE:
					closeAllCardsState();
					break;
				
				case CardValues.REMOVE_CALLBACK_STATE:
					this.removeCallbacksAndMessages(null);
					break;
			}
		}
	}
};

public void checkCards (final int number_card_1, final int number_card_2, 
							   final int index_card_1,  final int index_card_2) {
								   
	if (CardValues.cardsStates[index_card_1] != CardValues.CARD_STATE.CORRECT 
		&& CardValues.cardsStates[index_card_2] != CardValues.CARD_STATE.CORRECT) {
		
		if (number_card_1 == number_card_2) {
			
			CardValues.cardsStates[index_card_1] = CardValues.CARD_STATE.CORRECT;
			CardValues.cardsStates[index_card_2] = CardValues.CARD_STATE.CORRECT;
			cardGameIA.cardsCorrect++;

			if (cardGameIA.cardsCorrect == CardValues.maxCells >> 1) {
				win();
			}
			
		} else {
			
			final Card card1 = cards.get(index_card_1);
			
			if (card1 != null) {
				CardValues.cardsStates[index_card_1] = CardValues.CARD_STATE.CLOSED;
				createCardRunnable(card1);
			}

			final Card card2 = cards.get(index_card_2);
			
			if (card2 != null) {
				CardValues.cardsStates[index_card_2] = CardValues.CARD_STATE.CLOSED;
				createCardRunnable(card2);
			}
		}
		CardValues.state_card = CardValues.SELECT_CARD_1_STATE;
	}
	enableAction();
}

public void createCardRunnable (final Card card) {
	if (card != null) {
	
		if (cardHandler == null) {
			cardHandler = new Handler();
		}
		
		cardRunnable = new Runnable() {
			@Override
			public void run () {
				card.close();
			}
		};
		cardHandler.postDelayed(cardRunnable,CardValues.CARD_ANIMATION_TIME);
	}
}