public class GetTextRawFile {

    public static CharSequence getTextRawFile(@NonNull final Resources res, 
													   final int resId) {

        CharSequence charSequence = null;

        try {
            final InputStream raw = res.openRawResource(resId);

            if (raw != null && raw.available() > 0) {

                final byte[] reader = new byte[raw.available()];

                while (raw.read(reader) != -1) {
                    // lentidao ao ler ficheiros no raw
                }

                if (reader.length > 0) {
                    charSequence = new String(reader);
                }
                raw.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return charSequence;
    }
}
