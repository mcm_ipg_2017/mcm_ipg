package com.toymobi.framework.task;

public interface OnLoadTaskFinish {

    public void onLoadTaskFinish();
}
