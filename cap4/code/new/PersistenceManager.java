public class PersistenceManager {

    public static void saveSharedPreferencesString(@NonNull Context context,
                                                   final String key,
                                                   final String value) {
        if (key != null && value != null) {
            final SharedPreferences sharedPrefs = 
					PreferenceManager.getDefaultSharedPreferences(context);

            if (sharedPrefs != null) {
                final SharedPreferences.Editor editor = sharedPrefs.edit();
                if (editor != null) {
                    editor.putString(key, value);
                    editor.commit();
                }
            }
        }
    }

    public static String loadSharedPreferencesString(@NonNull Context context,
                                                     final String key,
                                                     final String defaultValue) {
        String value = defaultValue;

        if (key != null && defaultValue != null) {
            final SharedPreferences sharedPrefs = 
					PreferenceManager.getDefaultSharedPreferences(context);

            if (sharedPrefs != null) {
                value = sharedPrefs.getString(key, defaultValue);
            }
        }
        return value;
    }

    public static void saveSharedPreferencesInt(@NonNull Context context,
                                                final String key,
                                                final int value) { ... }

    public static int loadSharedPreferencesInt(@NonNull Context context,
                                               final String key,
                                               final int defaulValue) { ... }

    public static void saveSharedPreferencesBoolean(@NonNull Context context,
                                                    final String key,
                                                    final boolean value) { ... }

    public static boolean loadSharedPreferencesBoolean(@NonNull Context context,
                                                       final String key,
                                                       final boolean defaulValue) 
													   { ... }

}