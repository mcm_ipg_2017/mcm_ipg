public class DragAndDropGameView extends RelativeLayout implements OnTouchListener {

    public static final int[] SPOTS_RES_IDS = { R.id.spot1, R.id.spot2,
            R.id.spot3, R.id.spot4, R.id.spot5, R.id.spot6, R.id.spot7,
            R.id.spot8, R.id.spot9 };

    public static final int MAX_LETTER_SPOT = SPOTS_RES_IDS.length;

    private static final int OFFSET_TOUCH_POSITIVE = 32;

    private static final int OFFSET_TOUCH_NEGATIVE = -32;

    private SparseArrayCompat<ImageView> spotItems = null;

    private View selectedItemView = null;

    private int offsetX = 0, offsetY = 0;

    private boolean isTouch = false, isDrop = false;

    private android.view.ViewGroup.LayoutParams imageParams,
            imageParamsLetter1, imageParamsLetter2, imageParamsLetter3;

    private ImageView letter1, letter2, letter3;

    private int crashX, crashY;

    private int topy, leftX, rightX, bottomY;

    private int indexSpot = 0;

    private CharSequence checkCurrentLetterTag;

    private DragItem dragItem;

    public OnNextItemListener onNextItemListener;

    public DragAndDropGameView (final Context context) { ... }

    public DragAndDropGameView (final Context context, final AttributeSet attrs) { ... }

    public DragAndDropGameView (final Context context,
            final AttributeSet attrs, final int defStyle) { ... }

    @Override
    protected void onFinishInflate () { ... }

    private void init (final Context context) {
        if (!isInEditMode() && context != null) {

            inflate(context, R.layout.drag_and_drop_layout, this);

            if (spotItems == null) {

                final int lenght = SPOTS_RES_IDS.length;

                spotItems = new SparseArrayCompat<ImageView>(lenght);

                for (int i = 0; i < lenght; i++) {
                    final ImageView spot = (ImageView) findViewById(SPOTS_RES_IDS[i]);
                    if (spot != null) {
                        spotItems.put(i, spot);
                    }
                }
            }

            final ViewGroup container = (ViewGroup) findViewById(R.id.container);

            letter1 = (ImageView) findViewById(R.id.letter_1);

            letter2 = (ImageView) findViewById(R.id.letter_2);

            letter3 = (ImageView) findViewById(R.id.letter_3);

            final RelativeLayout.LayoutParams letterDragLayoutParams = new RelativeLayout.LayoutParams(
                    new ViewGroup.MarginLayoutParams(
                            RelativeLayout.LayoutParams.WRAP_CONTENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT));

            container.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);

            final int widthContainer = DeviceDimensionsUtil.getDisplayWidth(context);
            
            final int heightContainer = DeviceDimensionsUtil
                    .getDisplayHeight(context);

            final Rect rect = new Rect(OFFSET_TOUCH_NEGATIVE,
                    OFFSET_TOUCH_NEGATIVE, widthContainer
                            - OFFSET_TOUCH_POSITIVE, heightContainer
                            - OFFSET_TOUCH_POSITIVE);

            container.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch (final View view, final MotionEvent event) {

                    if (isTouch) {

                        final ImageView spotByIndex = spotItems.get(indexSpot);

                        switch (event.getActionMasked()) {

                        case MotionEvent.ACTION_DOWN:

                            if (spotByIndex != null) {

                                topy = spotByIndex.getTop();
                                leftX = spotByIndex.getLeft();
                                rightX = spotByIndex.getRight();
                                bottomY = spotByIndex.getBottom();
                            }

                            break;

                        case MotionEvent.ACTION_MOVE:
                            crashX = (int) event.getX();
                            crashY = (int) event.getY();

                            final int x = (int) event.getX() - offsetX;
                            final int y = (int) event.getY() - offsetY;

                            letterDragLayoutParams.setMargins(x, y, 0, 0);

                            // Drop Image Here
                            if (crashX > leftX && crashX < rightX
                                    && crashY > topy && crashY < bottomY) {

                                checkCurrentLetterTag = (CharSequence) selectedItemView
                                        .getTag();

                                if (checkCurrentLetterTag
                                        .equals(DragItem.CORRECT)) {
                                    if (dragItem != null) {
                                        if (spotByIndex != null) {

                                            spotByIndex
                                                    .setImageResource(dragItem.letterImageCorrect);

                                            if (onNextItemListener != null) {
                                                onNextItemListener.nextItem();
                                            }
                                        }
                                        isDrop = true;

                                        if (selectedItemView != null) {
                                            selectedItemView
                                                    .setVisibility(View.INVISIBLE);
                                        }
                                    }
                                } else {
                                    event.setAction(MotionEvent.ACTION_UP);
                                    onTouch(view, event);
                                }
                            } else {
                                if (selectedItemView != null) {
                                    // Drop Image Here
                                    if (rect != null && rect.contains(x, y)) {
                                        selectedItemView
                                                .setLayoutParams(letterDragLayoutParams);
                                    } else {
                                        event.setAction(MotionEvent.ACTION_UP);
                                        onTouch(view, event);
                                    }
                                }
                            }
                            break;
                        case MotionEvent.ACTION_UP:
                            isTouch = false;
                            if (isDrop) {
                                isDrop = false;
                            } else {
                                selectedItemView.setLayoutParams(imageParams);
                            }
                            break;
                        }
                    }
                    return true;
                }
            });

            letter1.setOnTouchListener(this);
            letter2.setOnTouchListener(this);
            letter3.setOnTouchListener(this);

            imageParamsLetter1 = letter1.getLayoutParams();
            imageParamsLetter2 = letter2.getLayoutParams();
            imageParamsLetter3 = letter3.getLayoutParams();
        }
    }

    @Override
    public boolean onTouch (final View view, final MotionEvent event) { ... }

    public final void deallocate () { ... }

    public final void setDragItem (final DragItem dragItem) { ... }

    private final void setAttributesIA () { ... }

    public final void eraserSpot () { ... }

    public final void start () { ... }
}
