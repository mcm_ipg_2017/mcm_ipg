package com.toymobi.framework.view.scrollview;

import ...

public class LockableHorizontalScrollView extends HorizontalScrollView {

    public LockableHorizontalScrollView (final Context context) {
        super(context);
    }

    public LockableHorizontalScrollView (final Context context,
            final AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override
    public boolean onTouchEvent (final MotionEvent motionEvent) {

		// correcao do projeto no scroll na vertical e horizontal
        if (motionEvent.getAction() == MotionEvent.ACTION_MOVE
                && getParent() != null) {
            getParent().requestDisallowInterceptTouchEvent(true);
        }
        return super.onTouchEvent(motionEvent);
    }

    @Override
    public boolean onInterceptTouchEvent (MotionEvent motionEvent) {
        if (getParent() != null) {
			
			// quando estiver movendo na horizontal bloqueia o scroll na vertical
            switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_MOVE:
                getParent().requestDisallowInterceptTouchEvent(true);
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                getParent().requestDisallowInterceptTouchEvent(false);
                break;
            }
        }
        return super.onInterceptTouchEvent(motionEvent);
    }
}